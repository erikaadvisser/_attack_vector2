import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router/router'

import './assets/css/bootstrap.css'
import './assets/css/datatables.css'
import './assets/css/ice.css'
import './assets/css/iziToast.min.css'
import './assets/css/jquery.terminal.css'
import './assets/css/jquery.mCustomScrollbar.css'
import './assets/css/terminal.css'
import './assets/css/local.css'




const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
