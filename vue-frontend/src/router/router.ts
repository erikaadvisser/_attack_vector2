import {createRouter, createWebHistory} from 'vue-router'
import Login from "@/routes/login/Login.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('@/routes/login/UserViewSwitcher.vue')
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        }

    ]
})

export default router
