import Cookies from "js-cookie";


export const removeCookies = () => {
    Cookies.remove("jwt");
    Cookies.remove("userName");
    Cookies.remove("type");
    Cookies.remove("icon");
    Cookies.remove("roles");
}