import React, {Component} from 'react';
import {removeCookies} from "./Cookies";

interface State { }
class Logoff extends Component<{}, State> {

    constructor(props: State) {
        super(props);
        removeCookies();
        document.location.href="/";
    }

    render() {
        return <div />;
    }

}

export default Logoff;
