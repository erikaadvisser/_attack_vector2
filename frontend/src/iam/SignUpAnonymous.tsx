import React, {Component} from 'react';
import {post} from "../common/RestClient";
import {HackerDisconnected} from "../common/player/HackerImage";
import Glyphicon from "../common/component/Glyphicon";
import {LEFT, RIGHT} from "../common/enums/LayerTypes";
import {shuffle} from "../common/CollectionsUtil";
import {PLAYER_NAMES} from "../common/PlayerNames";
import {ALL_PLAYER_ICONS, playerIconMap} from "../common/player/PlayerIconMap";

interface State {
    name: string,
    password: string,
    message: string,
    iconIndex: number,
    nameIndex: number,
}

class SignUpAnonymous extends Component<{}, State> {

    myHackerIcons: Array<string>
    myPlayerNames: Array<string>

    constructor(props: State) {
        super(props);
        this.myHackerIcons = shuffle([...ALL_PLAYER_ICONS])
        this.myPlayerNames = shuffle([...PLAYER_NAMES])
        this.state = {
            name: this.myPlayerNames[0],
            password: "",
            message: "",
            iconIndex: 0,
            nameIndex: 0
        };
    }

    onNameChange(value: string) {
        this.setState({name: value});
    }

    login() {
        const input = {name: this.state.name, icon: this.myHackerIcons[this.state.iconIndex]};
        this.setState({message: "Logging in"});

        post({
            url: "/api/signUpAnonymous",
            body: input,
            ok: ({success, message}: { success: boolean, message: string }) => {
                if (success) {
                    window.location.reload();
                } else {
                    this.setState({message: message});
                }
            },
            notok: (response: Response) => {
                this.setState({message: "Connection failed, unable to continue."});
            }
        });
    }


    moduloIndex(index: number, length: number): number {
        if (index < 0) {
            return index + length;
        }
        return index % length;
    }

    renderIcon(index: number, primary: boolean) {
        const icon = this.myHackerIcons[this.moduloIndex(index, this.myHackerIcons.length)];
        const fileName: string = playerIconMap[icon] ?? "question-mark.png";

        const style = primary ? {width: 68, height: 68} : {width: 45, height: 45, filter: "sepia(30%) blur(0.5px)"};


        return <span id={"id:" + index}><HackerDisconnected theme="frontier" type={icon} fileName={fileName}
                                                            style={style}/></span>

    }

    moveIcon(delta: number) {
        const newIndex = this.moduloIndex(this.state.iconIndex + delta, this.myHackerIcons.length);
        this.setState({...this.state, iconIndex: newIndex})
    }

    moveName(delta: number) {
        const newIndex = this.moduloIndex(this.state.nameIndex + delta, this.myPlayerNames.length)
        this.setState({...this.state, nameIndex: newIndex, name: this.myPlayerNames[newIndex]})
    }

    render() {
        return (
            <div className="container">
                <br/>
                <br/>
                <br/>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center text"><h4>Choose your representation</h4></div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        <button className="btn btn-grey" onClick={() => this.moveName(-1)}><Glyphicon type={LEFT}/></button>
                        &nbsp;&nbsp;
                        <span style={{width: 160, display: "inline-block"}}>&nbsp;

                            <input maxLength={10} type="text" className="form-control" id="handle" placeholder="" autoFocus={true}
                                   onChange={(event) => {
                                       this.onNameChange(event.target.value)
                                   }}
                                   value={this.state.name} style={{width: 150, display: "unset"}}/>
                               </span>
                        &nbsp;&nbsp;
                        <button className="btn btn-grey" onClick={() => this.moveName(1)}><Glyphicon type={RIGHT}/></button>
                    </div>
                </div>
                <br/>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        <button className="btn btn-grey" onClick={() => this.moveIcon(-1)}><Glyphicon type={LEFT}/></button>
                        &nbsp;&nbsp;
                        <span style={{width: 160}}>
                            {this.renderIcon(this.state.iconIndex - 1, false)}
                            {this.renderIcon(this.state.iconIndex, true)}
                            {this.renderIcon(this.state.iconIndex + 1, false)}
                        </span>
                        &nbsp;&nbsp;
                        <button className="btn btn-grey" onClick={() => this.moveIcon(1)}><Glyphicon type={RIGHT}/></button>
                    </div>
                </div>
                <br/>
                <br/>

                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        <button className="btn btn-info" onClick={() => this.login()}>Go</button>
                    </div>
                </div>
                <div className="row navbar-fixed-bottom" style={{bottom: 15}}>
                    <hr style={{borderTopColor: "#300", marginTop: "5px", marginBottom: "5px"}}/>
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        <a href="/about" target="_blank" ><i>about</i></a>
                    </div>
                    <br/>
                </div>

            </div>
        );
    }

}

export default SignUpAnonymous;

// style={{position: "absolute", bottom: 15, width: "90%"}}