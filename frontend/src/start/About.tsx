import React from "react";


export const About = () => {

    return (
        <div className="container">
            <br/>
            <div className="row">
                <div className="col-lg-offset-4 col-lg-4 text-center text"><h4>Attack Vector 2</h4></div>
            </div>
            <br/>
            <div className="row">
                <div className="col-lg-offset-4 col-lg-4 text ">
                    This is a puzzle and minigame platform designed for use in LARP events.<br/>
                    <br/>
                    Currently you are viewing a proof-of-concept of version 2.
                    It contains one puzzle that I call "tangle", originally it was called
                    Planarity (<a href="https://en.wikipedia.org/wiki/Planarity">wikipedia</a>).
                    My implementation was inspired by <a href="https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/untangle.html">Simon Tatham's version</a>.<br/>
                </div>
            </div>
            <br/>
            <div className="row">
                <div className="col-lg-offset-4 col-lg-4 text-center text"><h4>Open source</h4></div>
            </div>
            <div className="col-lg-offset-4 col-lg-4 text ">
                Attack Vector is open source, you can use it in any way you like, even commercially (MIT license). Source code is hosted
                on Bitbucket.<br/>
                <br/>
                <a href="https://www.youtube.com/watch?v=UrsL3WPik04">
                    Version 1</a> is a hacking game system used in the Dutch LARP: <a href="https://eosfrontier.space">Frontier</a>. (<a href="http://adventureforum.nl/ld50/attack_vector.html">More info</a>)<br/>
                <br/>
                Version 2 this work in progress (<a href="https://bitbucket.org/erikaadvisser/_attack_vector2">sources</a>).<br/>
                <br/>
                <br/>
                Contact me if you want to know more: <img src="/images/ld50email.png" alt=""/>
                <br/>
                <br/>
            </div>
            <div className="row">
                <div className="col-lg-offset-4 col-lg-4 text-center text"><h4>Privacy statement</h4></div>
            </div>
            <div className="col-lg-offset-4 col-lg-4 text ">
                The game code stores your username and icon as well as any game moves you make. The purpose is to test and improve gameplay and fix bugs.
                The game is hosted by <a href="https://www.heroku.com/free">Heroku free</a> on EU servers (<a href="https://www.heroku.com/policy/privacy">Heroku privacy policy</a>).
                <br/>
                <br/>
                <br/>
            </div>
            <div className="row">
                <div className="col-lg-offset-4 col-lg-4 text-center">
                    <button className="btn btn-grey" onClick={() => window.close()}>Close</button>&nbsp;
                </div>
            </div>
        </div>)

};