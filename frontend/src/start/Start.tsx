import React, {Component} from 'react';
import Cookies from "js-cookie";
import {PlayerIcon} from "../common/player/PlayerIcon";
import {Player} from "../common/player/PlayersReducer";

/* eslint jsx-a11y/alt-text: 0*/

interface State {
    url: string
}

class Start extends Component<{}, State> {

    urlBase: string
    player: Player

    constructor(props: State) {
        super(props);
        this.state = {
            url: ""
        };

        this.player = {
            userId: "-",
                name: Cookies.get("userName") ?? "unknown",
                icon: Cookies.get("icon") ?? "unknown",
                isYou: true
        }

        this.urlBase = window.location.protocol + "//" + window.location.host + "/p/t"
    }

    puzzleUrl(difficulty: number) {
        const id = Math.floor(Math.random() * 900) + 100

        this.setState({url: this.urlBase + difficulty + "" + id})
    }

    go() {
        window.location.href = this.state.url;
    }

    renderGo() {
        if (this.state.url) {
            return <button className="btn btn-info" onClick={() => this.go()}>Go</button>
        }
        else {
            return <button className="btn btn-info" disabled onClick={() => {}}>Go</button>
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <PlayerIcon player={this.player} />
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center text"><h4>Choose tangle puzzle difficulty</h4></div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        <button className="btn btn-primary" onClick={() => this.puzzleUrl(1)}>TUTORIAL</button>&nbsp;
                        <button className="btn btn-primary" onClick={() => this.puzzleUrl(2)}>SIMPLE</button>&nbsp;
                        <button className="btn btn-primary" onClick={() => this.puzzleUrl(3)}>MODERATE</button>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        <button className="btn btn-primary" onClick={() => this.puzzleUrl(4)}>HARD</button>&nbsp;
                        <button className="btn btn-primary" onClick={() => this.puzzleUrl(5)}>VERY_HARD</button>&nbsp;
                        <button className="btn btn-primary" onClick={() => this.puzzleUrl(6)}>EXTREME</button>
                    </div>
                </div>

                <br/>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center text"><h4>Puzzle URL</h4></div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                            <input type="text" className="form-control" autoFocus={true} value={this.state.url} readOnly/>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        {this.renderGo()}
                    </div>
                </div>
                <div className="row navbar-fixed-bottom" style={{bottom: 15}}>
                    <hr style={{borderTopColor: "#300", marginTop: "5px", marginBottom: "5px"}}/>
                    <div className="col-lg-offset-4 col-lg-4 text-center">
                        <a href="/about" target="_blank" ><i>about</i></a>
                    </div>
                    <br/>
                </div>

            </div>
        );
    }

}

export default Start;