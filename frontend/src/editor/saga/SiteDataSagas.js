import editorCanvas from "../component/middle/middle/EditorCanvas";
import webSocketConnection from "../../common/WebSocketConnection";
import {select} from 'redux-saga/effects'

const getSiteId = (state) => state.siteData.siteId;

function* requestSiteFullSaga(action) {
    webSocketConnection.send("editor/siteFull", action.siteId);
    yield
}

function* serverSiteFullSaga(action) {
    yield editorCanvas.loadSite(action.data);
}


function* editSiteDataSaga(action) {
    const siteId = yield select(getSiteId);
    let payload = {siteId: siteId, field: action.field, value: action.value};
    webSocketConnection.send("editor/editSiteData", payload);
    yield
}


export {requestSiteFullSaga, serverSiteFullSaga, editSiteDataSaga}