import editorCanvas from "../component/middle/middle/EditorCanvas";
import webSocketConnection from "../../common/WebSocketConnection";
import {select} from 'redux-saga/effects'

const getSiteId = (state) => state.siteData.siteId;


function* dropNodeSaga(action) {
    const siteId = yield select(getSiteId);
    let x = action.x - action.dragAndDropState.dx;
    let y = action.y - action.dragAndDropState.dy;
    let nodeType = action.dragAndDropState.type.name.toUpperCase();
    let payload = {siteId: siteId, x, y, type: nodeType};
    webSocketConnection.send("editor/addNode", payload);
    yield
}

function* serverNodeAddedSaga(action) {
    editorCanvas.addNode(action.data);
    editorCanvas.selectNode(action.data.id);
    yield
}


function* moveNodeSaga(action) {
    const siteId = yield select(getSiteId);
    let payload = {siteId: siteId, nodeId: action.id, x: action.x, y: action.y};
    webSocketConnection.send("editor/moveNode", payload);
    yield
}

function* serverMoveNodeSaga(action) {
    yield editorCanvas.moveNode(action.data);
}

function* addConnectionSaga(action) {
    const siteId = yield select(getSiteId);
    let payload = {siteId: siteId, fromId: action.fromId, toId: action.toId};
    webSocketConnection.send("editor/addConnection", payload);
    yield
}

function* serverAddConnectionSaga(action) {
    yield editorCanvas.addConnection(action.data);
}

function* deleteConnections(action) {
    const siteId = yield select(getSiteId);
    let payload = {siteId: siteId, nodeId: action.nodeId};
    webSocketConnection.send("editor/deleteConnections", payload);
    yield
}

function* deleteNode(action) {
    const siteId = yield select(getSiteId);
    let payload = {siteId: siteId, nodeId: action.nodeId};
    webSocketConnection.send("editor/deleteNode", payload);
    yield
}

function* snap(action) {
    const siteId = yield select(getSiteId);
    let payload = {siteId: siteId};
    webSocketConnection.send("editor/snap", payload);
    yield
}


export {
    dropNodeSaga, serverNodeAddedSaga,
    moveNodeSaga, serverMoveNodeSaga,
    addConnectionSaga, serverAddConnectionSaga,
    deleteConnections, deleteNode, snap
};

