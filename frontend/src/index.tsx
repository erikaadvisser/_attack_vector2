import React from 'react';
import {render} from 'react-dom'
import {BrowserRouter, Route, RouteComponentProps, Switch} from 'react-router-dom'
import EditorRoot from "./editor/EditorRoot";
import Login from "./iam/Login";
import HackerRoot from "./hacker/HackerRoot";
import Cookies from "js-cookie";
import TangleAdminRoot from "./standalone/tangle/admin/TangleAdminRoot";
import {ALL_USER_TYPES, USER_TYPE_ADMIN, USER_TYPE_ANONYMOUS, USER_TYPE_GM, USER_TYPE_USER} from "./iam/UsersAndRoles";
import SignUpAnonymous from "./iam/SignUpAnonymous";
import {removeCookies} from "./iam/Cookies";
import Start from "./start/Start"
import {About} from "./start/About";
import {TangleRoot} from "./standalone/tangle/game/TangleRoot";
import GmRoot from "./gm/GmRoot";


const ReRoute = (props: RouteComponentProps): React.ReactNode => {

    const type = Cookies.get("type");
    // if (type === USER_TYPE_ANONYMOUS) {
    //     window.document.location.href = "/start";
    //     return
    // }
    if (type === USER_TYPE_USER) {
        window.document.location.href = "/hacker/";
        return
    }
    if (type === USER_TYPE_GM) {
        window.document.location.href = "/gm/";
        return
    }
    if (type === USER_TYPE_ADMIN) {
        window.document.location.href = "/gm/admin";
        return
    }
    console.log("Unknown user type: " + type);
    window.document.location.href = "/logoff"
};

const type = Cookies.get("type") ?? "none"
const rootElement = document.getElementById('root');

if (window.location.pathname.startsWith("/about")) {
    render(<About />, rootElement)
}

else if (window.location.pathname.startsWith("/logoff")) {
    removeCookies();
    window.document.location.href = "/login"
// }
// else if (!ALL_USER_TYPES.includes(type)) {
//     render(<SignUpAnonymous/>, rootElement)

} else {
    render(
        <BrowserRouter>
            <Switch>
                <Route path="/start" component={Start}/>
                <Route path="/login" component={Login}/>
                <Route path="/hacker" component={HackerRoot}/>
                <Route path="/gm" component={GmRoot}/>
                <Route path="/anon" component={SignUpAnonymous}/>
                <Route path="/edit/:siteId?" component={EditorRoot}/>
                <Route path="/p/t:puzzleId?" component={TangleRoot}/>
                <Route path="/puzzle/tangle" component={TangleAdminRoot}/>
                <Route path="/" render={ReRoute}/>
            </Switch>
        </BrowserRouter>,
        rootElement
    );
}