import {SERVER_START_HACKING_ICE_PASSWORD} from "./password/PasswordIceActions";
import {ICE_PASSWORD, ICE_TANGLE} from "../../../common/enums/LayerTypes";
import {FINISH_HACKING_ICE} from "../model/HackActions";
import {SERVER_START_HACKING_ICE_TANGLE} from "./tangle/TangleIceActions";

const defaultState = {
    puzzleId: null,
    type: null
};

export interface CurrentIce {
    puzzleId: string | null,
    type: string | null
}


const CurrentIceReducer = (state = defaultState, action: any): CurrentIce => {
    switch (action.type) {
        case SERVER_START_HACKING_ICE_PASSWORD:
            return { puzzleId: action.data.id, type: ICE_PASSWORD };
        case SERVER_START_HACKING_ICE_TANGLE:
            return { puzzleId: action.data.puzzle.id, type: ICE_TANGLE };
        case FINISH_HACKING_ICE:
            return defaultState;
        default:
            return state;
    }
};

export default CurrentIceReducer
