import webSocketConnection from "../../../common/WebSocketConnection";
import {select} from 'redux-saga/effects'


const getCurrentIcePuzzleId = (state: any) => state.run.ice.currentIce.puzzleId;

export function* unsubscribePuzzle(action: any) {
    const puzzleId = yield select(getCurrentIcePuzzleId);
    if (puzzleId) {
        webSocketConnection.unsubscribeFromPuzzle(puzzleId);
    }
    yield
}