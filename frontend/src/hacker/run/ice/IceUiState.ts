
export const HIDDEN = "HIDDEN";
export const LOCKED = "LOCKED";
export const UNLOCKED = "UNLOCKED";

export type UiState = "HIDDEN" | "LOCKED" | "UNLOCKED";
