import {fabric} from "fabric";
import TangleLineDisplay from "./TangleLineDisplay";
import {TanglePoint} from "../TanglePuzzleModel";


const STROKE_FULL = undefined;
const STROKE_DENSE = [4, 2 ];
const STROKE_SPARSE = [2, 4 ];

const TANGLE_BLUE = "#337ab7";
const TANGLE_MEDIUM_BLUE = "#8ca6b7";
const TANGLE_LIGHT_BLUE = "#D6EEEE";
const TANGLE_WHITE = "#eee";

// const TANGLE_GREEN = "#57b72f";
const TANGLE_YELLOW = "#b7b42f";
const TANGLE_ORANGE = "#b78432";

interface PointStyles {
    fill: string,
    dash_array: Array<number> | undefined
}
interface PointStrengthStyle {
    clear: PointStyles,
    halfway: PointStyles,
    intersecting: PointStyles
}


const STYLE: Record<string, PointStrengthStyle> = {
    "VERY_WEAK": {
        clear: {
            dash_array: STROKE_FULL,
            fill: TANGLE_BLUE
        },
        halfway: {
            dash_array: STROKE_FULL,
            fill: TANGLE_ORANGE
        },
        intersecting: {
            dash_array: STROKE_FULL,
            fill: TANGLE_ORANGE
        }
    },
    "WEAK": {
        clear: {
            dash_array: STROKE_FULL,
            fill: TANGLE_BLUE
        },
        halfway: {
            dash_array: STROKE_DENSE,
            fill: TANGLE_YELLOW
        },
        intersecting: {
            dash_array: STROKE_SPARSE,
            fill: TANGLE_ORANGE
        }
    },
    "AVERAGE": {
        clear: {
            dash_array: STROKE_FULL,
            fill: TANGLE_BLUE
        },
        halfway: {
            dash_array: STROKE_DENSE,
            fill: TANGLE_MEDIUM_BLUE
        },
        intersecting: {
            dash_array: STROKE_SPARSE,
            fill: TANGLE_LIGHT_BLUE
        }
    },
    "STRONG": {
        clear: {
            dash_array: STROKE_FULL,
            fill: TANGLE_LIGHT_BLUE
        },
        halfway: {
            dash_array: STROKE_DENSE,
            fill: TANGLE_WHITE
        },
        intersecting: {
            dash_array: STROKE_SPARSE,
            fill: TANGLE_WHITE
        }
    },
    "VERY_STRONG": {
        clear: {
            dash_array: STROKE_DENSE,
            fill: TANGLE_WHITE
        },
        halfway: {
            dash_array: STROKE_DENSE,
            fill: TANGLE_WHITE
        },
        intersecting: {
            dash_array: STROKE_DENSE,
            fill: TANGLE_WHITE
        }
    },
    "IMPENETRABLE": {
        clear: {
            dash_array: STROKE_SPARSE,
            fill: TANGLE_WHITE
        },
        halfway: {
            dash_array: STROKE_SPARSE,
            fill: TANGLE_WHITE
        },
        intersecting: {
            dash_array: STROKE_SPARSE,
            fill: TANGLE_WHITE
        }
    },
};

export default class TanglePointDisplay {

    id: string;
    canvas: fabric.Canvas;
    lines: Array<TangleLineDisplay>;
    icon: fabric.Circle;
    strength: string;


    constructor(canvas: fabric.Canvas, pointData: TanglePoint, strength: string) {
        this.canvas = canvas;
        this.id = pointData.id;
        this.lines = [];
        this.strength = strength;

        this.icon = new fabric.Circle({
            radius: 6,
            top: pointData.y,
            left: pointData.x,
            fill: TANGLE_WHITE,
            stroke: "#000",
            // strokeWidth: 2,
            strokeDashArray: undefined,
            strokeWidth: 1,

            selectable: true,
            lockRotation: true,
            lockScalingX: true,
            lockScalingY: true,
            lockMovementX: false,
            lockMovementY: false,
            hoverCursor: "auto",
            moveCursor: "auto",
            hasBorders: false,
        });

        this.icon.data = { display: this };

        this.icon.setControlsVisibility({
            tl: false,
            tr: false,
            br: false,
            bl: false,
            ml: false,
            mt: false,
            mr: false,
            mb: false,
            mtr: false,
        });

        // this.idIcon = new fabric.Text("" + pointData.id, {
        //     fill: "#000",
        //     fontFamily: "SpaceMono",
        //     fontSize: 12,
        //     top: pointData.y + 10,
        //     left: pointData.x + 10,
        //     selectable: false,
        // })
    }

    show() {
        this.canvas.add(this.icon);
        // this.canvas.add(this.idIcon);
    }

    addLine(tangleLine: TangleLineDisplay) {
        this.lines.push(tangleLine);
    }

    highLight() {
        this.icon.set("strokeWidth", 4);
        // this.icon.set("fill", TANGLE_FILL_HIGHLIGHT_PRIMARY);

        this.lines.forEach(line => line.highlightOtherEnd(this));
    }

    secondaryHighlight() {
        this.icon.set("strokeWidth", 3);
        // this.icon.set("fill", TANGLE_FILL_HIGHLIGHT_SECONDARY);
    }

    unHighlight() {
        this.secondaryUnHighlight();
        this.lines.forEach(line => line.unHighlightOtherEnd(this));
    }

    secondaryUnHighlight() {
        this.icon.set("strokeWidth", 1);
        // this.icon.set("fill", TANGLE_FILL_NORMAL);
    }

    moved(x: number, y: number) {
        this.icon.set( { left: x, top: y} );
        this.icon.setCoords();

        // this.idIcon.setTop(y+10);
        // this.idIcon.setLeft(x+10);
        // this.idIcon.setCoords();

        this.updateLines();
    }

    updateLines() {
        this.lines.forEach(line => line.moved())
    }

    markNotIntersecting() {
        this.icon.set("strokeDashArray", STYLE[this.strength].clear.dash_array);
        this.icon.set("fill", STYLE[this.strength].clear.fill);

        // this.icon.set("strokeDashArray", STROKE_NON_INTERSECTING);
        // this.icon.set("fill", TANGLE_BLUE);
    }

    markHalfwayIntersecting() {
        this.icon.set("strokeDashArray", STYLE[this.strength].halfway.dash_array);
        this.icon.set("fill", STYLE[this.strength].halfway.fill);

        // this.icon.set("strokeDashArray", STROKE_HALFWAY);
        // this.icon.set("fill", TANGLE_LIGHT_BLUE);
    }

    markIntersecting() {
        this.icon.set("strokeDashArray", STYLE[this.strength].intersecting.dash_array);
        this.icon.set("fill", STYLE[this.strength].intersecting.fill);

        // this.icon.set("strokeDashArray", STROKE_INTERSECTING );
        // this.icon.set("fill", TANGLE_WHITE);
    }

}