import {fabric} from "fabric";
import TanglePointDisplay from "./TanglePointDisplay";




const DASH_CLEAR = undefined;
const DASH_REGULAR_TINY = [1,1];
const DASH_REGULAR_SHORT = [2,2];
const DASH_REGULAR_MEDIUM = [3,3];
const DASH_IRREGULAR_EASY = [3, 4, 3, 6, 3, 2];
const DASH_IRREGULAR_MEDIUM = [2, 4, 3, 6, 4, 2];
const DASH_IRREGULAR_HARD = [1, 4, 1, 6, 1, 2];

interface LineStyles {
    dash_array: Array<number> | undefined
    strokeWidth: number,
}
interface LineStrengthStyle {
    clear: LineStyles,
    intersecting: LineStyles
}
const STYLE: Record<string, LineStrengthStyle> = {
    "VERY_WEAK": {
        clear: {
            dash_array: DASH_CLEAR,
            strokeWidth: 2
        },
        intersecting: {
            dash_array: DASH_REGULAR_TINY,
            strokeWidth: 2,
        }
    },
    "WEAK": {
        clear: {
            dash_array: DASH_CLEAR,
            strokeWidth: 1
        },
        intersecting: {
            dash_array: DASH_REGULAR_SHORT,
            strokeWidth: 1,
        }
    },
    "AVERAGE": {
        clear: {
            dash_array: DASH_CLEAR,
            strokeWidth: 1
        },
        intersecting: {
            dash_array: DASH_REGULAR_MEDIUM,
            strokeWidth: 1,
        }
    },
    "STRONG": {
        clear: {
            dash_array: DASH_IRREGULAR_EASY,
            strokeWidth: 1
        },
        intersecting: {
            dash_array: DASH_IRREGULAR_EASY,
            strokeWidth: 1,
        }
    },
    "VERY_STRONG": {
        clear: {
            dash_array: DASH_IRREGULAR_EASY,
            strokeWidth: 1
        },
        intersecting: {
            dash_array: DASH_IRREGULAR_MEDIUM,
            strokeWidth: 1,
        }
    },
    "IMPENETRABLE": {
        clear: {
            dash_array: DASH_IRREGULAR_HARD,
            strokeWidth: 1
        },
        intersecting: {
            dash_array: DASH_IRREGULAR_HARD,
            strokeWidth: 1,
        }
    },};

export default class TangleLineDisplay {

    canvas: fabric.Canvas;
    id: string;
    point1: TanglePointDisplay;
    point2: TanglePointDisplay;
    strength: string;
    icon: fabric.Line;

    constructor(canvas: fabric.Canvas, id: string, point1: TanglePointDisplay, point2: TanglePointDisplay, strength: string) {
        this.canvas = canvas;
        this.id = id;
        this.point1 = point1;
        this.point2 = point2;
        this.strength = strength;

        this.icon = new fabric.Line(
            [
                this.point1.icon.left as number,
                this.point1.icon.top as number,
                this.point2.icon.left as number,
                this.point2.icon.top as number], {
                stroke: "#333",
                strokeDashArray: DASH_CLEAR,
                strokeWidth: 2,
                selectable: false,
                hoverCursor: 'default',
            });

        // this.idIcon = new fabric.Text("" + id, {
        //     fill: "#000",
        //     fontFamily: "SpaceMono",
        //     fontSize: 12,
        //     top: (this.point1.icon.top + this.point2.icon.top) / 2,
        //     left: (this.point1.icon.left + this.point2.icon.left) / 2,
        //     selectable: false,
        //     });
    }

    show() {
        this.canvas.add(this.icon);
        // this.canvas.add(this.idIcon);
    }

    highlightOtherEnd(pointDisplay: TanglePointDisplay) {
        this.findOther(pointDisplay).secondaryHighlight();
        this.highLight();
    }

    unHighlightOtherEnd(pointDisplay: TanglePointDisplay) {
        this.findOther(pointDisplay).secondaryUnHighlight();
        this.unHighlight();
    }

    findOther(pointDisplay: TanglePointDisplay) {
        return (pointDisplay === this.point1) ? this.point2 : this.point1;
    }

    highLight() {
        this.icon.set("stroke", "#337ab7");
    }

    unHighlight() {
        this.icon.set("stroke", "#333");
    }

    moved() {
        const newCoordinates = {
            x1: this.point1.icon.left,
            y1: this.point1.icon.top,
            x2: this.point2.icon.left,
            y2: this.point2.icon.top
        };
        this.icon.set(newCoordinates);
        this.icon.setCoords();

        // this.idIcon.top = (this.point1.icon.top + this.point2.icon.top) / 2;
        // this.idIcon.left = (this.point1.icon.left + this.point2.icon.left) / 2;
        // this.idIcon.setCoords();


    }

    markIntersecting() {
        this.icon.set("strokeDashArray", STYLE[this.strength].intersecting.dash_array);
        this.icon.set("strokeWidth", STYLE[this.strength].intersecting.strokeWidth);
        // this.icon.set("strokeDashArray", DASH_ARRAY_INTERSECTING);
        // this.icon.set("strokeWidth", 1)

    }

    markNotIntersecting() {
        this.icon.set("strokeDashArray", STYLE[this.strength].clear.dash_array);
        this.icon.set("strokeWidth", STYLE[this.strength].clear.strokeWidth);
        // this.icon.set("strokeDashArray", DASH_ARRAY_CLEAR);
        // this.icon.set("strokeWidth", 1)
    }
}