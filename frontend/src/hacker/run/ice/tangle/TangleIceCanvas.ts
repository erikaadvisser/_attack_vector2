import {fabric} from "fabric";
import TanglePointDisplay from "./display/TanglePointDisplay";
import TangleLineDisplay from "./display/TangleLineDisplay";
import {ICE_TANGLE_MOVE_POINT} from "./TangleIceActions";
import {Dispatch} from "redux";
import {Canvas} from "fabric/fabric-impl";
import {TangleLine, TanglePoint, TanglePointMoved, TanglePuzzleStart, TanglePuzzleStatus} from "./TanglePuzzleModel";


export default class TangleIceCanvas {

    dispatch: Dispatch;
    canvas: Canvas;
    pointDisplayById: Record<string, TanglePointDisplay> = {};
    lineDisplaysById: Record<string, TangleLineDisplay> = {};
    currentSelected: TanglePointDisplay | null = null;


    constructor(data: TanglePuzzleStart, dispatch: Dispatch) {
        this.dispatch = dispatch;

        this.canvas = new fabric.Canvas('untangleCanvas', {backgroundColor: "#aaa", targetFindTolerance: 15});
        this.canvas.setDimensions({width: 1200, height: 680});

        fabric.Object.prototype.originX = "center";
        fabric.Object.prototype.originY = 'center';

        const canvas = this.canvas;

        setTimeout(function () {
            fabric.Image.fromURL("/images/frontier/ice/tangle/fractal-untangle-1200x680.png", (img) => {
                img.set({width: canvas.getWidth(), height: canvas.getHeight(), originX: 'left', originY: 'top'});
                canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
            });
        }, 100);

        // this.canvas.on('object:modified', (event) => { this.canvasObjectModified(event); });
        this.canvas.on('object:selected', (event) => {
            this.canvasObjectSelected(event);
        });
        this.canvas.on('selection:cleared', (event) => {
            this.canvasObjectDeSelected(event);
        });
        this.canvas.on('mouse:up', (event) => {
            this.canvasObjectDeSelected(event);
        });
        this.canvas.on('object:moving', (event) => {
            this.canvasObjectMoved(event);
        });
        this.canvas.selection = false;

        const {points, lines, strength} = data.puzzle;

        this.pointDisplayById = {};

        points.forEach(point => this.addPoint(point, strength));
        lines.forEach(line => this.addLine(line, strength));
        Object.values(this.pointDisplayById).forEach(pointDisplay => pointDisplay.show());
        this.updateForStatus(data.status);

        this.canvas.discardActiveObject();
        this.canvas.requestRenderAll();
    }

    addPoint(pointData: TanglePoint, strength: string) {
        this.pointDisplayById[pointData.id] = new TanglePointDisplay(this.canvas, pointData, strength);
    }

    addLine(lineData: TangleLine, strength: string) {
        const fromDisplay = this.pointDisplayById[lineData.fromId];
        const toDisplay = this.pointDisplayById[lineData.toId];

        const lineDisplay = new TangleLineDisplay(this.canvas, lineData.id, fromDisplay, toDisplay, strength);
        this.lineDisplaysById[lineData.id] = lineDisplay;

        fromDisplay.addLine(lineDisplay);
        toDisplay.addLine(lineDisplay);

        lineDisplay.show();
    }

    canvasObjectSelected(event: fabric.IEvent) {
        if (event.target?.data['display']) {
            this.currentSelected = event.target.data['display'] as TanglePointDisplay;
            this.currentSelected.highLight();
            this.canvas.requestRenderAll();
        }
    }

    canvasObjectDeSelected(event: fabric.IEvent) {
        if (this.currentSelected) {
            const icon = this.currentSelected.icon;
            this.dispatch({type: ICE_TANGLE_MOVE_POINT, id: this.currentSelected.id, x: icon.left, y: icon.top});
            this.currentSelected.unHighlight();
            this.currentSelected = null;
            this.canvas.discardActiveObject();
            this.canvas.requestRenderAll();
        }
    }

    canvasObjectMoved(event: fabric.IEvent) {
        if (event.target?.data['display']) {
            const display = event.target.data['display'] as TanglePointDisplay;
            const id = display.id;
            const pointDisplay = this.pointDisplayById[id];
            pointDisplay.updateLines();
            this.canvas.requestRenderAll();
        }
    }

    serverMovedPoint(actionData: TanglePointMoved) {
        this.pointDisplayById[actionData.id].moved(actionData.x, actionData.y);
        this.updateForStatus(actionData.status);
        this.canvas.requestRenderAll();
    }

    updateForStatus(status: TanglePuzzleStatus) {

        Object.values(this.pointDisplayById).forEach(pointDisplay => pointDisplay.markNotIntersecting());

        status.halfwayPointIds
            .map(pointId => this.pointDisplayById[pointId])
            .forEach(pointDisplay => pointDisplay.markHalfwayIntersecting());

        status.intersectingPointIds
            .map(pointId => this.pointDisplayById[pointId])
            .forEach(pointDisplay => pointDisplay.markIntersecting());

        Object.values(this.lineDisplaysById).forEach(lineDisplay => lineDisplay.markNotIntersecting());
        status.intersectingLineIds
            .map(lineId => this.lineDisplaysById[lineId])
            .forEach(lineDisplay => lineDisplay.markIntersecting());

    }
}

