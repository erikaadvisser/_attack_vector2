import getTangleIceManager from "./TangleIceManager";
import {select} from 'redux-saga/effects'
import webSocketConnection from "../../../../common/WebSocketConnection";
import {ActionTangleMovePoint, TanglePointMoved, TanglePuzzleSolved, TanglePuzzleStart} from "./TanglePuzzleModel";
import {CurrentIce} from "../CurrentIceReducer";

const getCurrentIce = (state: any) => state.run.ice.currentIce;

interface TangleStartPuzzleMessage {
    data: TanglePuzzleStart
}
export function* tangleIceStartHack(action: TangleStartPuzzleMessage) {
    webSocketConnection.subscribeForPuzzle(action.data.puzzle.id);
    yield getTangleIceManager().startHack(action.data);
}

export function* tangleIceMovePoint(action: ActionTangleMovePoint) {
    const currentIce: CurrentIce = yield select(getCurrentIce);

    const payload = {puzzleId: currentIce.puzzleId, pointId: action.id, newX: action.x, newY: action.y};
    webSocketConnection.send("puzzle/tangle/moved", payload);
    yield

}


interface TanglePointMovedMessage {
    data: TanglePointMoved
}
export function* tanglePointMoved(action: TanglePointMovedMessage) {
    const currentIce: CurrentIce = yield select(getCurrentIce);
    if (currentIce.puzzleId === action.data.puzzleId) {
        getTangleIceManager().moved(action.data);
    }
    yield
}


interface TanglePuzzleSolvedMessage {
    data: TanglePuzzleSolved
}
export function* serverTanglePuzzleSolved(action: TanglePuzzleSolvedMessage) {
    const currentIce: CurrentIce = yield select(getCurrentIce);
    if (currentIce.puzzleId === action.data.puzzleId) {
        getTangleIceManager().solved();
    }
    yield
}