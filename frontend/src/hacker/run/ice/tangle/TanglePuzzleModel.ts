// ---- Fundamentals ---- //

export interface TanglePuzzle {
    id: string,
    solved: boolean,
    strength: string,
    points: Array<TanglePoint>,
    lines: Array<TangleLine>
}

export interface TanglePoint {
    id: string,
    x: number,
    y: number
}

export interface TangleLine {
    id: string,
    fromId: string,
    toId: string
}


// ---- Actions ---- //

export interface ActionTangleMovePoint {
    id: string, // point id
    x: number,
    y: number
}


// ---- Message data ---- //

export interface TanglePuzzleStatus {
    intersectingPointIds: Array<string>,
    halfwayPointIds: Array<string>,
    intersectingLineIds: Array<string>
}

export interface TanglePuzzleStart {
    puzzle: TanglePuzzle,
    status: TanglePuzzleStatus
}

export interface TanglePointMoved {
    puzzleId: string,
    id: string,
    x: number,
    y: number,
    status: TanglePuzzleStatus

}

export interface TanglePuzzleSolved {
    puzzleId: String
}

