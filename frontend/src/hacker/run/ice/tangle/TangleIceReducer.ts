import {HIDDEN, UiState, UNLOCKED} from "../IceUiState";
import {ICE_TANGLE_BEGIN, SERVER_START_HACKING_ICE_TANGLE} from "./TangleIceActions";
import {ICE_TANGLE} from "../../../../common/enums/LayerTypes";
import {CurrentIce} from "../CurrentIceReducer";
import {TanglePuzzleStart} from "./TanglePuzzleModel";

export interface TangleIce {
    strength: string,
    uiState: UiState
}
const defaultState: TangleIce = {
    strength: "AVERAGE",
    uiState: HIDDEN,
};

const TangleIceReducer = (state = defaultState, action: any, currentIce: CurrentIce) => {
    if (!currentIce ||  currentIce.type !== ICE_TANGLE) {
        return
    }

    switch (action.type) {
        case SERVER_START_HACKING_ICE_TANGLE:
            const data = action.data as TanglePuzzleStart;
            return { ...data.puzzle, uiState: HIDDEN };
        case ICE_TANGLE_BEGIN:
            return { ...state, uiState: UNLOCKED };
        default:
            return state;
    }
};



export default TangleIceReducer
