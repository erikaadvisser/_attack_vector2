import {combineReducers} from 'redux'
import iceRootReducer from "../../../hacker/run/ice/IceRootReducer";
import countdownReducer from "../../../hacker/run/coundown/CountdownReducer";
import themeReducer from "../../../common/reducer/ThemeReducer";
import userIdReducer from "../../../common/reducer/UserIdReducer";

const saTangleRunRootReducer = combineReducers({
    ice: iceRootReducer,
    countdown: countdownReducer
});

const hackerRootReducer = combineReducers({
    run: saTangleRunRootReducer,
    theme: themeReducer,
    userId: userIdReducer

});

export default hackerRootReducer;