import TangleLineDisplay from "./TangleLineDisplay";
import {TanglePoint} from "./TanglePuzzleModel";
import * as PIXI from 'pixi.js'
import {addDragDrop} from "./AddDragDrop";
import {Dispatch} from "redux";
import {ICE_TANGLE_MOVE_POINT} from "./TangleIceActions";
import {getUnscaledPosition, setPositionScaled} from "../../../common/Scaler";

const UNIFORMS_NORMAL = 4.6;
const UNIFORMS_HL_PRIMARY = 5.2;
const UNIFORMS_HL_SECONDARY = 3.4



export default class TanglePointDisplay {

    id: string;
    stage: PIXI.Container;
    lines: Array<TangleLineDisplay> = [];
    icon: PIXI.Graphics;
    strength: string;
    dispatch: Dispatch;
    readonly uniforms = {time: 4.6};


    constructor(dispatch: Dispatch, stage: PIXI.Container, pointData: TanglePoint, strength: string) {
        this.dispatch = dispatch;
        this.stage = stage;
        this.id = pointData.id;
        this.lines = [];
        this.strength = strength;

        this.icon = new PIXI.Graphics();

        this.icon.beginFill(0xe74c3c);
        this.icon.drawCircle(0, 0, 6); // drawCircle(x, y, radius)
        this.icon.endFill();
        this.icon.zIndex = 1;

        // if (Math.random() > 0.5) {
        //     this.icon.scale.x = 2;
        //
        // }
        // this.icon.position.x = pointData.x - OFFSET;
        // this.icon.position.y = pointData.y - OFFSET;
        setPositionScaled(this.icon.position, pointData.x, pointData.y);


        // this.icon.x = pointData.x - OFFSET;
        // this.icon.y = pointData.y - OFFSET;
        this.icon.interactive = true;
        this.icon.hitArea = new PIXI.Circle(0, 0, 20);


        const colorizing = `
precision mediump float;

uniform float time;
varying vec2 vTextureCoord;
uniform sampler2D uSampler;

void main(){
   gl_FragColor = texture2D(uSampler, vTextureCoord);
   if (gl_FragColor.a != 0.0) {
       gl_FragColor.r = sin(time + vTextureCoord.x);
       gl_FragColor.g = cos(time + vTextureCoord.y);
       gl_FragColor.b = cos(0.5 * 3.14 + time + vTextureCoord.x);
   }

}`;

//         const red = `
// void main()
// {
//     gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
// }
//         `;


        const filter = new PIXI.Filter('', colorizing, this.uniforms);
        this.icon.filters = [filter];
        stage.addChild(this.icon);

        // Force display property in spite of type incompatibility
        // (this.icon as unknown as CircleIcon).display = this;

        addDragDrop(this.icon,
            () => {
                this.highLight();
            },
            () => {
                this.lines.forEach(line => line.moved())
            },
            () => {
                const realPos = getUnscaledPosition(this.icon.position);
                // console.log("system started: " + PIXI.Ticker.system.started + ", fps: " + PIXI.Ticker.system.FPS);
                this.unHighlight();
                this.dispatch({type: ICE_TANGLE_MOVE_POINT, id: this.id, x: realPos.x, y: realPos.y});

            });

        // setInterval(() => {
        //     uniforms.time += 0.1;
        // }, 10);

    }

    show() {
        this.stage.addChild(this.icon);
    }

    addLine(tangleLine: TangleLineDisplay) {
        this.lines.push(tangleLine);
    }

    highLight() {
        this.uniforms.time = UNIFORMS_HL_PRIMARY;
        // this.icon.set("strokeWidth", 4);
        // this.icon.set("fill", TANGLE_FILL_HIGHLIGHT_PRIMARY);

        this.lines.forEach(line => line.highlightOtherEnd(this));
    }

    secondaryHighlight() {
        this.uniforms.time = UNIFORMS_HL_SECONDARY;
        // this.icon.set("strokeWidth", 3);
        // this.icon.set("fill", TANGLE_FILL_HIGHLIGHT_SECONDARY);
    }

    unHighlight(primary = true) {
        this.uniforms.time = UNIFORMS_NORMAL;
        if (primary) {
            this.lines.forEach(line => line.unHighlightOtherEnd(this));
        }
    }


    moved(x: number, y: number) {
        setPositionScaled(this.icon.position, x, y);
        // this.icon.x = x;
        // this.icon.y = y;
        // this.icon.set( { left: x, top: y} );
        // this.icon.setCoords();

        // this.idIcon.setTop(y+10);
        // this.idIcon.setLeft(x+10);
        // this.idIcon.setCoords();

        this.updateLines();
    }

    updateLines() {
        this.lines.forEach(line => line.moved())
    }

    markNotIntersecting() {
        // this.icon.set("strokeDashArray", STYLE[this.strength].clear.dash_array);
        // this.icon.set("fill", STYLE[this.strength].clear.fill);

        // this.icon.set("strokeDashArray", STROKE_NON_INTERSECTING);
        // this.icon.set("fill", TANGLE_BLUE);
    }

    markHalfwayIntersecting() {
        // this.icon.set("strokeDashArray", STYLE[this.strength].halfway.dash_array);
        // this.icon.set("fill", STYLE[this.strength].halfway.fill);

        // this.icon.set("strokeDashArray", STROKE_HALFWAY);
        // this.icon.set("fill", TANGLE_LIGHT_BLUE);
    }

    markIntersecting() {
        // this.icon.set("strokeDashArray", STYLE[this.strength].intersecting.dash_array);
        // this.icon.set("fill", STYLE[this.strength].intersecting.fill);

        // this.icon.set("strokeDashArray", STROKE_INTERSECTING );
        // this.icon.set("fill", TANGLE_WHITE);
    }

}