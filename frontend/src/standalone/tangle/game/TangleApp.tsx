import React from 'react';
import Header from "./Header";


// const toFullScreen = () => {
//
//     const container = document.getElementById('fullScreenContainer') as Element;
//
//     container.requestFullscreen()
//         .then( () => {
//             setTimeout(() => {
//                 tangleCanvas.resize();
//             }, 240);
//
//
//         })
//         .catch(err => {
//         alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
//     })
//
// };

export function TangleApp() {
  return (
    <div className="App" id="fullScreenContainer">
        <Header/>
        <canvas id="pixiCanvas" width="250" height="250" />
    </div>
  );
}
