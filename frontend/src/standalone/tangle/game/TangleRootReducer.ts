import {combineReducers} from 'redux'
import {tanglePuzzleReducer} from "./TangleReducer";
import {uiReducer, UiState} from "../../../common/UiReducer";
import {TanglePuzzle} from "./TanglePuzzleModel";
import playersReducer, {Players} from "../../../common/player/PlayersReducer";



export interface RootState {
    puzzle: TanglePuzzle,
    ui: UiState,
    players: Players,
}

export const tangleRootReducer = combineReducers({
    puzzle: tanglePuzzleReducer,
    ui: uiReducer,
    players: playersReducer
});
