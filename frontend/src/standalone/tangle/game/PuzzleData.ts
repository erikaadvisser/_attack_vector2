import {TanglePuzzle} from "./TanglePuzzleModel";



const puzzleData: TanglePuzzle = {
    "id" : "tangle-92b7",
    "solved" : true,
    "strength" : "WEAK",
    "points" : [
    {
        "id" : "p-0",
        "x" : 389,
        "y" : 322
    },
    {
        "id" : "p-1",
        "x" : 738,
        "y" : 441
    },
    {
        "id" : "p-2",
        "x" : 70,
        "y" : 91
    },
    {
        "id" : "p-3",
        "x" : 573,
        "y" : 419
    },
    {
        "id" : "p-4",
        "x" : 638,
        "y" : 457
    },
    {
        "id" : "p-5",
        "x" : 85,
        "y" : 460
    },
    {
        "id" : "p-6",
        "x" : 652,
        "y" : 269
    },
    {
        "id" : "p-7",
        "x" : 584,
        "y" : 212
    },
    {
        "id" : "p-8",
        "x" : 188,
        "y" : 325
    },
    {
        "id" : "p-9",
        "x" : 167,
        "y" : 500
    },
    {
        "id" : "p-10",
        "x" : 700,
        "y" : 574
    },
    {
        "id" : "p-11",
        "x" : 292,
        "y" : 24
    },
    {
        "id" : "p-12",
        "x" : 345,
        "y" : 487
    },
    {
        "id" : "p-13",
        "x" : 572,
        "y" : 489
    },
    {
        "id" : "p-14",
        "x" : 491,
        "y" : 426
    },
    {
        "id" : "p-15",
        "x" : 451,
        "y" : 457
    },
    {
        "id" : "p-16",
        "x" : 265,
        "y" : 223
    },
    {
        "id" : "p-17",
        "x" : 619,
        "y" : 62
    },
    {
        "id" : "p-18",
        "x" : 90,
        "y" : 540
    },
    {
        "id" : "p-19",
        "x" : 561,
        "y" : 538
    },
    {
        "id" : "p-20",
        "x" : 246,
        "y" : 646
    }
],
    "lines" : [
    {
        "id" : "l-0",
        "fromId" : "p-10",
        "toId" : "p-1"
    },
    {
        "id" : "l-1",
        "fromId" : "p-1",
        "toId" : "p-6"
    },
    {
        "id" : "l-2",
        "fromId" : "p-6",
        "toId" : "p-17"
    },
    {
        "id" : "l-3",
        "fromId" : "p-17",
        "toId" : "p-16"
    },
    {
        "id" : "l-4",
        "fromId" : "p-16",
        "toId" : "p-2"
    },
    {
        "id" : "l-5",
        "fromId" : "p-18",
        "toId" : "p-20"
    },
    {
        "id" : "l-6",
        "fromId" : "p-20",
        "toId" : "p-12"
    },
    {
        "id" : "l-7",
        "fromId" : "p-12",
        "toId" : "p-15"
    },
    {
        "id" : "l-8",
        "fromId" : "p-15",
        "toId" : "p-19"
    },
    {
        "id" : "l-9",
        "fromId" : "p-19",
        "toId" : "p-10"
    },
    {
        "id" : "l-10",
        "fromId" : "p-13",
        "toId" : "p-15"
    },
    {
        "id" : "l-11",
        "fromId" : "p-15",
        "toId" : "p-14"
    },
    {
        "id" : "l-12",
        "fromId" : "p-14",
        "toId" : "p-0"
    },
    {
        "id" : "l-13",
        "fromId" : "p-0",
        "toId" : "p-8"
    },
    {
        "id" : "l-14",
        "fromId" : "p-8",
        "toId" : "p-2"
    },
    {
        "id" : "l-15",
        "fromId" : "p-9",
        "toId" : "p-12"
    },
    {
        "id" : "l-16",
        "fromId" : "p-12",
        "toId" : "p-14"
    },
    {
        "id" : "l-17",
        "fromId" : "p-14",
        "toId" : "p-3"
    },
    {
        "id" : "l-18",
        "fromId" : "p-3",
        "toId" : "p-7"
    },
    {
        "id" : "l-19",
        "fromId" : "p-7",
        "toId" : "p-6"
    },
    {
        "id" : "l-20",
        "fromId" : "p-13",
        "toId" : "p-19"
    },
    {
        "id" : "l-21",
        "fromId" : "p-19",
        "toId" : "p-4"
    },
    {
        "id" : "l-22",
        "fromId" : "p-4",
        "toId" : "p-7"
    },
    {
        "id" : "l-23",
        "fromId" : "p-7",
        "toId" : "p-17"
    },
    {
        "id" : "l-24",
        "fromId" : "p-17",
        "toId" : "p-11"
    },
    {
        "id" : "l-25",
        "fromId" : "p-18",
        "toId" : "p-5"
    },
    {
        "id" : "l-26",
        "fromId" : "p-5",
        "toId" : "p-0"
    },
    {
        "id" : "l-27",
        "fromId" : "p-0",
        "toId" : "p-3"
    },
    {
        "id" : "l-28",
        "fromId" : "p-3",
        "toId" : "p-4"
    },
    {
        "id" : "l-29",
        "fromId" : "p-4",
        "toId" : "p-1"
    },
    {
        "id" : "l-30",
        "fromId" : "p-9",
        "toId" : "p-20"
    },
    {
        "id" : "l-31",
        "fromId" : "p-20",
        "toId" : "p-5"
    },
    {
        "id" : "l-32",
        "fromId" : "p-5",
        "toId" : "p-8"
    },
    {
        "id" : "l-33",
        "fromId" : "p-8",
        "toId" : "p-16"
    },
    {
        "id" : "l-34",
        "fromId" : "p-16",
        "toId" : "p-11"
    }
]};

export {puzzleData}