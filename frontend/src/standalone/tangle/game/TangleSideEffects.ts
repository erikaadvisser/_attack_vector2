import {registerEffect} from "../../../common/SideEffects";
import webSocketConnection from "../../../common/WebSocketConnection";
import {tangleCanvas} from "./TangleCanvas";
import {Dispatch} from "redux";
import {ICE_TANGLE_MOVE_POINT, SERVER_START_TANGLE_STANDALONE, SERVER_TANGLE_POINT_MOVED, SERVER_TANGLE_SOLVED} from "./TangleIceActions";
import {SERVER_NOTIFICATION} from "../../../common/enums/CommonActions";
import {notify} from "../../../common/Notification";


const registerEffects = (dispatch: Dispatch, puzzleId: String) => {

    registerEffect('START', (state: any, action: any) => {
        // webSocketConnection.send('puzzle/tangle/getAll', "");
        webSocketConnection.send('puzzle/tangle/demo', action.puzzle_id);
    });

    registerEffect(SERVER_START_TANGLE_STANDALONE, (state: any, action: any) => {
        tangleCanvas.init(dispatch, action.data);
    });

    registerEffect(ICE_TANGLE_MOVE_POINT, (state: any, action: any) => {
        const payload = {puzzleId: puzzleId, pointId: action.id, newX: action.x, newY: action.y};
        webSocketConnection.send("puzzle/tangle/moved", payload);
    });

    registerEffect(SERVER_TANGLE_POINT_MOVED, (state: any, action: any) => {
        tangleCanvas.moved(action.data);
    });


    registerEffect(SERVER_NOTIFICATION, (state: any, action: any) => {
        notify(action.data);
    });

    registerEffect(SERVER_TANGLE_SOLVED, (state: any, action: any) => {
        notify({type: "ok", title:"Success", message:"Solution found!"});
    });

    registerEffect("TANGLE_RESTART", (state: any, action: any) => {
        webSocketConnection.send("puzzle/tangle/demoRestart", puzzleId);
    });

    registerEffect("blur", (state: any, action: any) => {
        webSocketConnection.send("puzzle/tangle/blur", "dummy");
    });

    registerEffect("focus", (state: any, action: any) => {
        webSocketConnection.send("puzzle/tangle/focus", "dummy");
    });



};

export default registerEffects;
