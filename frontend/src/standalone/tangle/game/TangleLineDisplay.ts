import TanglePointDisplay from "./TanglePointDisplay";
import * as PIXI from 'pixi.js'


const COLOR_NORMAL = 0xaaaaaa;
const COLOR_SELECTED = 0x00bca0;
// const a = "#00bca0";

export default class TangleLineDisplay {

    stage: PIXI.Container;
    id: string;
    point1: TanglePointDisplay;
    point2: TanglePointDisplay;
    strength: string;
    icon: PIXI.Graphics;
    currentColor = COLOR_NORMAL;

    constructor(stage: PIXI.Container, id: string, point1: TanglePointDisplay, point2: TanglePointDisplay, strength: string) {
        this.stage = stage;
        this.id = id;
        this.point1 = point1;
        this.point2 = point2;
        this.strength = strength;

        this.icon = new PIXI.Graphics();
        this.redraw();
        // this.icon.lineStyle(1, COLOR_NORMAL, 1);
        // this.icon.moveTo(0, 0);
        // this.icon.lineTo(0 + this.point2.icon.x -this.point1.icon.x , 0 + this.point2.icon.y -this.point1.icon.y);
        // this.icon.x = this.point1.icon.x;
        // this.icon.y = this.point1.icon.y;
        this.icon.zIndex = 0;

        this.stage.addChild(this.icon);

    }

    redraw() {
        this.icon.clear();
        this.icon.lineStyle(1, this.currentColor, 1);
        this.icon.moveTo(0, 0);
        this.icon.lineTo(this.point2.icon.x -this.point1.icon.x , this.point2.icon.y -this.point1.icon.y);
        this.icon.x = this.point1.icon.x;
        this.icon.y = this.point1.icon.y;
    }

    show() {
        // this.stage.add(this.icon);
        // this.canvas.add(this.idIcon);
    }

    highlightOtherEnd(pointDisplay: TanglePointDisplay) {
        this.findOther(pointDisplay).secondaryHighlight();
        this.highLight();
    }

    unHighlightOtherEnd(pointDisplay: TanglePointDisplay) {
        this.findOther(pointDisplay).unHighlight(false);
        this.unHighlight();
    }

    findOther(pointDisplay: TanglePointDisplay) {
        return (pointDisplay === this.point1) ? this.point2 : this.point1;
    }

    highLight() {
        this.currentColor = COLOR_SELECTED;
        // this.icon.set("stroke", "#337ab7");
    }

    unHighlight() {
        this.currentColor = COLOR_NORMAL;
        // this.icon.set("stroke", "#333");
    }

    moved() {
        this.redraw();

        // const newCoordinates = {
        //     x1: this.point1.icon.left,
        //     y1: this.point1.icon.top,
        //     x2: this.point2.icon.left,
        //     y2: this.point2.icon.top
        // };
        // this.icon.set(newCoordinates);
        // this.icon.setCoords();

        // this.idIcon.top = (this.point1.icon.top + this.point2.icon.top) / 2;
        // this.idIcon.left = (this.point1.icon.left + this.point2.icon.left) / 2;
        // this.idIcon.setCoords();


    }

    markIntersecting() {
        // this.icon.set("strokeDashArray", STYLE[this.strength].intersecting.dash_array);
        // this.icon.set("strokeWidth", STYLE[this.strength].intersecting.strokeWidth);

    }

    markNotIntersecting() {
        // this.icon.set("strokeDashArray", STYLE[this.strength].clear.dash_array);
        // this.icon.set("strokeWidth", STYLE[this.strength].clear.strokeWidth);
    }
}