import * as PIXI from "pixi.js";


const addDragDrop = (target: PIXI.Graphics,
                     start: () => void = () => {},
                     move: () => void = () => {},
                     end: () => void = () => {}) => {

    let deltaX = 0;
    let deltaY = 0;

    const onDragMove = (event: PIXI.interaction.InteractionEvent) => {
        const newPosition = event.data.getLocalPosition(target.parent);
        newPosition.x -= deltaX;
        newPosition.y -= deltaY;

        if (newPosition.x === target.position.x && newPosition.y === target.position.y) {
            return;
        }

        target.position.x = newPosition.x;
        target.position.y = newPosition.y;
        // console.log("new: (" + newPosition.x + "," + newPosition.y + ") delta(" + deltaX + "," + deltaY + ")");
        move();
    };

    const onDragStart = (event: PIXI.interaction.InteractionEvent) => {
        target
            .on('pointermove', onDragMove);

        deltaX = event.data.global.x - event.target.x;
        deltaY = event.data.global.y - event.target.y;
        start();
    };

    const onDragEnd = (event: any) => {
        target.removeListener('pointermove');
        end();
    };

    target.on('pointerdown', onDragStart)
        .on('pointerup', onDragEnd)
        .on('pointerupoutside', onDragEnd)
};

export {addDragDrop}

