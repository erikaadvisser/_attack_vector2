import React, {Component} from 'react';
import {applyMiddleware, compose, createStore, Store} from "redux";
import {Provider} from "react-redux";
import webSocketConnection from "../../../common/WebSocketConnection";
import {EffectsMiddleware} from "../../../common/SideEffects";
import registerEffects from "./TangleSideEffects";
import {TangleApp} from "./TangleApp";
import {tangleRootReducer} from "./TangleRootReducer";

interface State {}

export class TangleRoot extends Component<{}, State> {

    store: Store

    constructor(props: State) {
        super(props);
        const path = window.location.pathname;
        if (path.length !== 8 || !path.startsWith("/p/t")) {
            alert("Invalid path / URL");
            throw Error("Invalid URL: " + path)
        }
        const  puzzle_id = "tangle-" + path.substr(4);

        const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        this.store = createStore(tangleRootReducer, {}, composeEnhancers(applyMiddleware(EffectsMiddleware)));
        registerEffects(this.store.dispatch, puzzle_id);
        setTimeout(() => {
            webSocketConnection.create(this.store, () => {
                this.store.dispatch({type: "START", puzzle_id: puzzle_id});
                webSocketConnection.subscribeForPuzzle(puzzle_id);
            });
        }, 25);

        document.onvisibilitychange = (ev) => {
            const eventType = document.hidden ? "blur" : "focus";
            this.store.dispatch({type: eventType})
        };

    }

    render() {
        return (
            <Provider store={this.store}>
                <div className="container" id="container">
                    <TangleApp/>
                </div>
            </Provider>
        )
    }
}
