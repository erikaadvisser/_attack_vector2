import React from 'react';
import {connect} from "react-redux";
import {Dispatch} from "redux";
import {TangleAdminPage, TangleAdminState} from "../TangleAdminRootReducers";
import SilentLink from "../../../../common/component/SilentLink";
import {TanglePuzzleInfo, TanglePuzzles} from "../TanglePuzzlesReducer";
import {NAVIGATE_PAGE} from "../../../../common/enums/CommonActions";
import webSocketConnection from "../../../../common/WebSocketConnection";

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        createNew: () => {
            dispatch({type: NAVIGATE_PAGE, to: TangleAdminPage.CREATE});
            webSocketConnection.send('puzzle/tangle/demo', "");
        }
    }
};

let mapStateToProps = (state: TangleAdminState) => {
    return {
        puzzles: state.puzzles
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(
    ({puzzles, createNew}: { puzzles: TanglePuzzles, createNew: () => void }) => {

        return (
            <div className="row">
                <div className="col-lg-2">
                    <span className="text">&nbsp;</span>
                    <div className="row">
                        <div className="text">
                            <br/>
                            <br/>
                        </div>
                        <div id="actions">
                            <div className="text">
                                <button className="btn btn-default" onClick={createNew}>Create new</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col-lg-8">
                    <div>&nbsp;</div>
                    <table className="table table-condensed text-muted text" id="sitesTable">
                        <thead>
                        <tr>
                            <td className="strong">Puzzle link</td>
                            <td className="strong">Difficulty</td>
                            <td className="strong">Description</td>
                            <td className="strong">Type</td>
                            <td className="strong">Players</td>
                            <td className="strong">&nbsp;</td>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            puzzles.map((puzzle: TanglePuzzleInfo) => {
                                return (
                                    <tr key={puzzle.id}>
                                        <td className="table-very-condensed">
                                            <SilentLink onClick={() => {
                                                alert('click')
                                            }}>{puzzle.id}</SilentLink>
                                        </td>
                                        <td className="table-very-condensed">{puzzle.strength}</td>
                                        <td className="table-very-condensed">{puzzle.description}</td>
                                        <td className="table-very-condensed">{puzzle.type}</td>
                                        <td className="table-very-condensed">{puzzle.activePlayers}</td>
                                        <td className="table-very-condensed">
                                            <SilentLink title={puzzle.strength} onClick={() => {
                                                alert('delete')
                                            }}>
                                                <span className="glyphicon glyphicon-remove-circle"/>
                                            </SilentLink>
                                        </td>
                                    </tr>);
                            })
                        }
                        </tbody>
                    </table>
                </div>
                <div className="col-lg-2">
                    <span className="text">&nbsp;</span>
                </div>

            </div>
        );
    });
