import React from "react";
import {Dispatch} from "redux";
import {TangleAdminPage, TangleAdminState} from "./TangleAdminRootReducers";
import TangleAdminHome from "./home/TangleAdminHome";
import TangleCreate from "./create/TangleCreate";
import {connect} from "react-redux";


const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
    };
};
let mapStateToProps = (state: TangleAdminState) => {
    return {
        page: state.page,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(
    ({page}: { page: TangleAdminPage }) => {

    switch (page) {
        case 'CREATE':
            return <TangleCreate />;
        default:
            return <TangleAdminHome />;
    }
});