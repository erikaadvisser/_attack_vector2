import {combineReducers} from 'redux'
import {TanglePuzzles, tanglePuzzlesReducer} from "./TanglePuzzlesReducer";
import pageReducer from "../../../common/reducer/pageReducer";


export enum TangleAdminPage {
    HOME = 'HOME',
    CREATE = 'CREATE',
}

export interface TangleAdminState {
    puzzles: TanglePuzzles,
    page: TangleAdminPage
    // puzzleParams: TanglePuzzleInfo
}

const tangleAdminRootReducer = combineReducers({
    puzzles: tanglePuzzlesReducer,
    page: pageReducer

});

export default tangleAdminRootReducer;