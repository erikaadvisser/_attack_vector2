import {ReduxAction} from "../../../common/CommonTypes";

export enum IceStrength {
    VERY_WEAK = 'VERY_WEAK',
    WEAK = 'WEAK',
    AVERAGE = 'AVERAGE',
    STRONG = 'STRONG',
    VERY_STRONG = 'VERY_STRONG',
    INPENETRABLE = 'INPENETRABLE'
}

export enum PuzzleUsageType {
    ICE = 'ICE',
    STANDALONE = 'STANDALONE'
}

export interface TanglePuzzleInfo {
    id: string,
    strength: IceStrength,
    description: string,
    activePlayers: number,
    type: PuzzleUsageType,
}

export type TanglePuzzles = Array<TanglePuzzleInfo>


export const tanglePuzzlesReducer = (state: TanglePuzzles = [], action: ReduxAction) => {
    switch (action.type) {
        case "SERVER_LIST_TANGLES": {
            const data: Array<TanglePuzzleInfo> = action.data;

            const allPuzzles = [];
            for (let i = 0; i < 4; i++) {
                allPuzzles.push(data[0])
            }
            return allPuzzles
        }
        default:
            return state;
    }
};
