import React from 'react';
import {connect} from "react-redux";
import {Dispatch} from "redux";
import {IceStrength, PuzzleUsageType, TanglePuzzleInfo} from "../TanglePuzzlesReducer";
import TextInput from "../../../../common/component/TextInput";

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {}
};
let mapStateToProps = (state: any) => {
    return {
        puzzleInfo: {
            id: "tangle-0000",
            strength: IceStrength.WEAK,
            description: "TEST test test",
            activePlayers: 0,
            type: PuzzleUsageType.STANDALONE,
        }
    };
};

interface Props {
    puzzleInfo: TanglePuzzleInfo
}

const component: React.FC<Props> = (props: Props) => {

    // const {puzzleInfo} = props;

    // const classHidden = ice.uiState === HIDDEN ? " hidden_alpha" : "";
    const classHidden = "";

    return (
        <>
        <div className="row">
            <div className="col-lg-2">
                <TextInput placeholder="Description"
                           buttonLabel="Create"
                           buttonClass="btn-info"
                           save={(description: string) => alert(description)}
                           clearAfterSubmit="true"/>
            </div>
        </div>

        <div className="row untangleIcePanelRow">
            <div className="col-lg-12">
                <div className="row">
                    <div className="col-lg-12">
                    </div>
                </div>
                <hr style={{borderTopColor: "#300", marginTop: "5px", marginBottom: "5px"}}/>

                <div className={"row transition_alpha_fast" + classHidden}>
                    <div className="col-lg-12">
                        <div>
                            <canvas id="untangleCanvas" style={{
                                "borderRadius": "3px 3px 3px 3px",
                                "marginTop": "10px",
                                "marginBottom": "10px",
                            }}/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        </>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(component);
