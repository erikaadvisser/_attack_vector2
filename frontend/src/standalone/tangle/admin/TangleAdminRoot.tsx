import React, {Component} from "react";
import {applyMiddleware, compose, createStore, Store} from "redux";
import {Provider} from "react-redux";
import tangleAdminRootReducer, {TangleAdminState} from "./TangleAdminRootReducers";
import webSocketConnection from "../../../common/WebSocketConnection";
import {EffectsMiddleware, registerEffect} from "./TangleEffects";
import TangleAdminPageChooser from "./TangleAdminPageChooser";
import getTangleIceManager, {initTangleIceManager} from "../../../hacker/run/ice/tangle/TangleIceManager";


registerEffect('LIST_TANGLES', (state: TangleAdminState, action: any) => {
    webSocketConnection.send('puzzle/tangle/getAll', "");
    webSocketConnection.send('puzzle/tangle/demo', "");
});

registerEffect('SERVER_START_HACKING_ICE_TANGLE', (state: TangleAdminState, action: any) => {
    getTangleIceManager().startHack(action.data);
});

export default class TangleAdminRoot extends Component {

    store: Store;

    constructor(props: Readonly<{}>) {
        super(props);

        const rootReducer = tangleAdminRootReducer;

        const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        this.store = createStore(rootReducer, {}, composeEnhancers(applyMiddleware(EffectsMiddleware)));

        initTangleIceManager(this.store);


        webSocketConnection.create(this.store, () => {
            this.store.dispatch({type: "LIST_TANGLES"});
        });

    }

    render() {
        return (
            <Provider store={this.store}>
                <div className="container">
                    <TangleAdminPageChooser/>
                </div>
            </Provider>
        );
    }

}