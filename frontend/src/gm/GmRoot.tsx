import React, { Component } from 'react'
import GmHome from "./component/GmHome";
import { Provider } from 'react-redux'
import RequiresRole from "../iam/RequiresRole";
import {createStore, Store} from "redux";
import gmReducer from "./GmReducer";
import {PAGE_GM_SITES} from "./GmPages";
import {fetchSites} from "./FetchSites";

class GmRoot extends Component<{}, {}> {

    store: Store = createStore(gmReducer, {currentPage: PAGE_GM_SITES});

    constructor(props: {}) {
        super(props);
        fetchSites(this.store.dispatch);
    }

    render() {
        return(
            <RequiresRole requires="ROLE_SITE_MANAGER">
                <Provider store={this.store}>
                    <GmHome />
                </Provider>
            </RequiresRole>
        )
    }

}

export default GmRoot

