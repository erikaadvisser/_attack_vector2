
export interface GmState {
    sites: Array<GmSite>
}

export interface GmSite {
    id: string,
    name: string
}