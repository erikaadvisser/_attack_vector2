import {combineReducers} from 'redux'
import pageReducer from "../common/reducer/pageReducer";
import sitesReducer from "./GmSitesReducer"


export const gmReducer = combineReducers({
    currentPage: pageReducer,
    sites: sitesReducer,
});

