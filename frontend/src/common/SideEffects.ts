import {Dispatch} from "redux";


/** Middleware that performs side effects such as sending signals to the server */

export type SideEffect = (state: any, action: any) => void;

const sideEffects: Record<string, SideEffect> = {};


export const registerEffect = (type: string, effect: SideEffect) => {
    sideEffects[type] = effect;
};

export const EffectsMiddleware = ({getState, dispatch}: { getState: () => any, dispatch: Dispatch }) => {
    return (next: (arg0: any) => any) => (action: any) => {
        // console.log('will dispatch', action);

        const preState = getState();

        const sideEffect = sideEffects[action.type];
        if (sideEffect !== undefined) {
            sideEffect(preState, action);
        }

        // Call the next dispatch method in the middleware chain.
        const returnValue = next(action);

        // console.log('state after dispatch', getState());

        // This will likely be the action itself, unless
        // a middleware further in chain changed it.
        return returnValue
    }
};