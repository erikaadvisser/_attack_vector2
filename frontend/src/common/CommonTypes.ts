

export interface ReduxAction {
    type: string,
    [propName: string]: any;
}