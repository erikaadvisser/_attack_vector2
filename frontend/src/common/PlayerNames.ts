export const PLAYER_NAMES = [
    "Aberration", "Absence", "Ace", "Alpha", "Angel", "Anomaly", "Answer", "Archangel", "Aspect", "Atom", "Avatar", "Behemoth", "Beta", "Bishop", "Bite",
    "Blank", "Blazer", "Bliss", "Boggle", "Bullet", "Bullseye", "Burn", "Catch-22", "Chaos", "Charm", "Chase", "Chief", "Chimera", "Chronicle", "Cipher",
    "Claw", "Combo", "Comet", "Conjurer", "Cowboy", "Craze", "Crotchet", "Crypto", "Cryptonic", "Curse", "Dagger", "Dante", "Dexter", "Diablo", "Drake",
    "Ecstasy", "Epitome", "Essence", "Flash", "Flinch", "Fury", "Ghoul", "Gloom", "Grace", "Grimace", "Guru", "Habit", "Haze", "Hex", "Hoax", "Hollow",
    "Ibis", "Idol", "Indie", "Infinity", "Jinx", "Kiss", "Knot", "Law", "Legacy", "Lightning", "Limbo", "Lynx", "Marshall", "Mascot", "Matriarch", "Memento",
    "Memory", "Mermaid", "Mongoose", "Moonlight", "Moonshine", "Morgana", "Mothership", "Myth", "Nemesis", "Neurosis", "Nighthawk", "Nightmare", "Nightowl",
    "Nix", "Oblivion", "Oddity", "Omega", "Onyx", "Oracle", "Owl", "Panther", "Paradox", "Paragon", "Parody", "Particle", "Perplex", "Phantasm", "Phase",
    "Phoenix", "Pierce", "Prankster", "Pride", "PrimaDonna", "Prophecy", "Proto", "Proxy", "Quake", "Rapture", "Rebus", "Reverse", "Rider", "Rogue", "Rune",
    "Sabertooth", "Sage", "Savant", "Scepter", "Sentinel", "Serenity", "Shade", "Shadow", "Shark", "Shepherd", "Shield", "Sickle", "Silver", "Skipper", "Smog",
    "Specter", "Sphinx", "Spider", "Splinter", "Spook", "Squid", "Stalker", "Storm", "Sunshine", "Surprise", "Swan", "Talisman", "Thief", "Tinge", "Torpedo",
    "Trace", "Trail", "Tremor", "Trinity", "Trix", "Trust", "Umbra", "Vacuum", "Vagabond", "Veil", "Vermin", "Vestige", "Viper", "Vision", "Void", "Voyage",
    "Webster", "Wolf", "Zigzag", "Zion",
];

