import {IziToastPosition} from "izitoast";

const iziToast = require("izitoast");

const notify_ok = (title: string, message: string, position: IziToastPosition) => {
    iziToast.show({
        title: title,
        message: message,
        position: position,
        color: 'green',
    });
};

const notify_neutral = (title: string, message: string) => {
    iziToast.show({
        title: title,
        message: message,
        position: 'topCenter',
        color: 'yellow'
    });
};

const notify_error = (title: string, message: string) => {
    iziToast.show({
        title: title,
        message: message,
        position: 'topCenter',
        color: 'blue',
    });
};

const notify_fatal = (message: string) => {
    iziToast.show({
        title: 'Fatal',
        message: message,
        position: 'topCenter',
        color: 'red',
        timeout: false
    });
};


const notify_advice = (title: string, message: string, position: IziToastPosition) => {
    iziToast.show({
        title: title,
        message: message,
        position: position,
        color: 'green',
        timeout: false
    });
};

type NotifyType = 'ok' | 'ok_right' | 'advice_right' | 'advice_left' | 'neutral' | 'error' | 'fatal';

const notify = ({type, title, message}: {type: NotifyType, title: string, message: string}) => {

    if (type === "ok") {
        notify_ok(title, message, "topCenter");
    }
    else if (type === "ok_right") {
        notify_ok(title, message, "topRight");
    }
    else if (type === "advice_right") {
        notify_advice(title, message, "topRight");
    }
    else if (type === "advice_left") {
        notify_neutral(title, message);
    }
    else if (type === "neutral") {
        notify_neutral(title, message);
    }
    else if (type === "error") {
        notify_error(title, message);
    }
    else {
        notify_fatal(title + " " + message);
    }
};

export { notify, notify_neutral, notify_fatal }