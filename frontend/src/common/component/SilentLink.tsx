import React from 'react';
import {connect} from "react-redux";

/**
 * This goal of this component is to prevent the displaying of the link URL over the menu bar at the bottom of the screen.
 *
 * Props:
 * - className
 * - href     (choose one)
 * - onClick  (choose one)
 * - children (implicit)
 * - title
 */

export type LinkClickEvent = React.MouseEvent<HTMLAnchorElement, MouseEvent> | Event;

const mapDispatchToProps = () => {
    return {}
};
let mapStateToProps = (state: any) => {
    return {};
};

interface Props {
    href?: string,
    classNameInput?: string,
    onClick?: (event: LinkClickEvent) => void,
    title?: string,
    children?: React.ReactNode,
}

const component: React.FC<Props> = (props) => {
    const {href, classNameInput = "", children, onClick, title} = props;

    const onClickLocal = (e: LinkClickEvent) => {
        if (onClick) {
            onClick(e);
            return;
        }
        if (href) {
            window.location.href = href;
        }
    };

    const className = "silentLink " + classNameInput;

    // eslint-disable-next-line
    return (<a onClick={(e) => onClickLocal(e)} className={className} title={title}>{children}</a>)
};

export default connect(mapStateToProps, mapDispatchToProps)(component);
