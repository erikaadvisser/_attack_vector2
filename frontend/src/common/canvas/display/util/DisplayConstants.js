export const SIZE_NORMAL = 40;
export const SIZE_SMALL = 20;
export const SIZE_LARGE = 100;

export const OFFSET = 20;

export const IDENTIFICATION_SIZE_LARGE = 60;
export const IDENTIFICATION_SIZE_NORMAL = SIZE_NORMAL;


export const COLOR_PROBE_LINE = "#285ba0";
export const COLOR_PATROLLER_LINE = "#A42433";
export const COLOR_HACKER_LINE = "#d6c540";


export const TICKS_HACKER_MOVE_MAIN = 16;
export const TICKS_HACKER_MOVE_START = 4;
export const TICKS_HACKER_MOVE_END = 4;
