import createSagaMiddleware, {Saga} from "redux-saga";
import {Action, applyMiddleware, compose, createStore, Store} from "redux";
import webSocketConnection from "./WebSocketConnection";


export const createWebsocketRootWithSagas = (rootReducer: any, rootSaga: Saga, initialDispatch: Action): Store => {
    const sagaMiddleware = createSagaMiddleware();

    const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(rootReducer, {}, composeEnhancers(applyMiddleware(sagaMiddleware)));
    // this.store = createStore(hackerRootReducer, preLoadedState, applyMiddleware(sagaMiddleware));

    webSocketConnection.create(store, () => {
        store.dispatch(initialDispatch);
    });

    sagaMiddleware.run(rootSaga);
    return store;
};

export const createWebsocketRootWithoutSagas= (rootReducer: any, initialDispatch: Action): Store => {

    const store = createStore(rootReducer, {}, (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());
    webSocketConnection.create(store, () => {
        store.dispatch(initialDispatch);
    });

    return store;
};
