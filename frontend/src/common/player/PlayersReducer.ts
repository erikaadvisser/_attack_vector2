import {AnyAction} from "redux";
import {SERVER_START_TANGLE_STANDALONE} from "../../standalone/tangle/game/TangleIceActions";

export interface Player {
    userId: string,
    name: string,
    icon: string,
    isYou: boolean
}

export type Players = Array<Player>;


const defaultState: Players = [];

const playersReducer = (state = defaultState, action: AnyAction) => {
    switch (action.type) {
        case SERVER_START_TANGLE_STANDALONE:
            return action.data.players;
        default:
            return state;
    }
};


export default playersReducer