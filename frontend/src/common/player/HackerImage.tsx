import React from 'react';
import {connect} from "react-redux";

/* eslint jsx-a11y/alt-text: 0*/

const mapDispatchToProps = () => {
    return {}
};

const mapStateToProps = (state: any) => {
    if (state) {
        return {
            theme: state.theme
        };
    }
    else {
        return {theme: "frontier"}
    }
};

interface Props {
    theme: string,
    fileName: string,
    type: string,
    onLoad?: () => void
    style?: Record<string, any>
}



export const HackerDisconnected: React.FC<Props> = ({theme, fileName, type, onLoad, style}) => {
    const root = "/images/" + theme + "/actor/player/";
    const dirAndName = root + fileName;

    if (!style) {
        style = {};
    }

    if (!onLoad) {
        onLoad = () => {};
    }

    return (
        <img src={dirAndName} id={type} onLoad={onLoad} alt="" style={style}/>
    )
};

export const HackerImage = connect(mapStateToProps, mapDispatchToProps)(HackerDisconnected)


