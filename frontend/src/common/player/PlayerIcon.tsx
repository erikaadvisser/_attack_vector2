import React from "react";
import {Player} from "./PlayersReducer";
import {iconToPath} from "./PlayerIconMap";


interface Props {
    player: Player,
}

export const PlayerIcon: React.FC<Props> = ({player}) => {
    const source = iconToPath(player.icon);
    const className = player.isYou ? "playerIconYou" : "playerIconOther"
    return <img src={source} className={className} alt=""/>
};
