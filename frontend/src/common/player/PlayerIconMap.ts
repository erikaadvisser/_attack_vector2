export const ANT_ICON = "ANT_ICON";
export const BEAR_1 = "BEAR_1";
export const BEAR_ICON = "BEAR_ICON";
export const BEAR_2 = "BEAR_2";
export const BIRD_1 = "BIRD_1";
export const BIRD_SMALL = "BIRD_SMALL";
export const BIRD_OF_PREY = "BIRD_OF_PREY";
export const BIRD_EAGLE_ATTACK = "BIRD_EAGLE_ATTACK";
export const BIRD_EAGLE = "BIRD_EAGLE";
export const BIRD_DOVE_ICON = "BIRD_DOVE_ICON";
export const BIRD_DUCK_FLIGHT = "BIRD_DUCK_FLIGHT";
export const BIRD_HERON = "BIRD_HERON";
export const BIRD_SMALL_ICON = "BIRD_SMALL_ICON";
export const BULL = "BULL";
export const BULL_ICON = "BULL_ICON";
export const BUTTERFLY_1 = "BUTTERFLY_1";
export const BUTTERFLY_2 = "BUTTERFLY_2";
export const BUTTERFLY_3 = "BUTTERFLY_3";
export const CAMEL = "CAMEL";
export const DROMEDARY = "DROMEDARY";
export const CAT_PAW = "CAT_PAW";
export const CAT_SIT = "CAT_SIT";
export const CAT_ICON_CUTE_1 = "CAT_ICON_CUTE_1";
export const CAT_ICON_CUTE_2 = "CAT_ICON_CUTE_2";
export const CAT_ICON_CUTE_3 = "CAT_ICON_CUTE_3";
export const CAT_ICON_CUTE_4 = "CAT_ICON_CUTE_4";
export const CAT_ICON_CUTE_5 = "CAT_ICON_CUTE_5";
export const CAT_ICON_CUTE_6 = "CAT_ICON_CUTE_6";
export const CAT_PLAY = "CAT_PLAY";
export const CAT_ICON = "CAT_ICON";
export const CAT_KITTEN = "CAT_KITTEN";
export const CAT_ICON_CUTE_7 = "CAT_ICON_CUTE_7";
export const CAT_ICON_CUTE_8 = "CAT_ICON_CUTE_8";
export const COW = "COW";
export const COW_ICON = "COW_ICON";
export const CRAB = "CRAB";
export const CROCODILE = "CROCODILE";
export const DEER_FEMALE = "DEER_FEMALE";
export const DEER_JUMP = "DEER_JUMP";
export const DEER_MALE = "DEER_MALE";
export const DINO_T_REX = "DINO_T_REX";
export const DINO_BRONTO = "DINO_BRONTO";
export const DINO_TRICERATOPS = "DINO_TRICERATOPS";
export const DINE_STEGGO = "DINE_STEGGO";
export const DOG_PAW = "DOG_PAW";
export const DOG_ICON_LEASH = "DOG_ICON_LEASH";
export const DOG_STAND = "DOG_STAND";
export const DOG_PUPPY = "DOG_PUPPY";
export const DOG_ICON_CUTE = "DOG_ICON_CUTE";
export const DOG_2 = "DOG_2";
export const DRAGON_1 = "DRAGON_1";
export const DRAGON_2 = "DRAGON_2";
export const DRAGON_3 = "DRAGON_3";
export const DRAGON_4 = "DRAGON_4";
export const DRAGON_5 = "DRAGON_5";
export const DRAGONFLY = "DRAGONFLY";
export const DUCK = "DUCK";
export const DUCK_ICON = "DUCK_ICON";
export const ELEPHANT_FRONT = "ELEPHANT_FRONT";
export const ELEPHANT_RIGHT = "ELEPHANT_RIGHT";
export const ELEPHANT_LEFT = "ELEPHANT_LEFT";
export const FISH_1 = "FISH_1";
export const FISH_2 = "FISH_2";
export const FISH_3 = "FISH_3";
export const FISH_STINGRAY = "FISH_STINGRAY";
export const FISH_SHARK = "FISH_SHARK";
export const FROG = "FROG";
export const GIRAFFE = "GIRAFFE";
export const GIRAFFE_ICON = "GIRAFFE_ICON";
export const HAMSTER_ICON = "HAMSTER_ICON";
export const RHINO_RIGHT = "RHINO_RIGHT";
export const RHINO_LEFT = "RHINO_LEFT";
export const HORSE_1 = "HORSE_1";
export const HORSE_2 = "HORSE_2";
export const KANGAROO = "KANGAROO";
export const KOALA = "KOALA";
export const LADYBUG_1 = "LADYBUG_1";
export const LADYBUG_2 = "LADYBUG_2";
export const LADYBUG_ICON = "LADYBUG_ICON";
export const LADYBUG_3 = "LADYBUG_3";
export const LADYBUG_4 = "LADYBUG_4";
export const LADYBUG_5 = "LADYBUG_5";
export const LION_1 = "LION_1";
export const LION_BANNER = "LION_BANNER";
export const LION_2 = "LION_2";
export const LION_3 = "LION_3";
export const LIZARD_1 = "LIZARD_1";
export const LIZARD_2 = "LIZARD_2";
export const LOBSTER_1 = "LOBSTER_1";
export const LOBSTER_2 = "LOBSTER_2";
export const MONKEY_1 = "MONKEY_1";
export const MONKEY_2 = "MONKEY_2";
export const MOOSE = "MOOSE";
export const MOUSE = "MOUSE";
export const PANDA_1 = "PANDA_1";
export const PANDA_2 = "PANDA_2";
export const PAW_PRINTS_1 = "PAW_PRINTS_1";
export const PENGUIN = "PENGUIN";
export const RABBIT = "RABBIT";
export const RABBIT_ICON = "RABBIT_ICON";
export const SEAHORSE_1 = "SEAHORSE_1";
export const SEAHORSE_2 = "SEAHORSE_2";
export const SEAL = "SEAL";
export const SHELLFISH_1 = "SHELLFISH_1";
export const SHELLFISH_2 = "SHELLFISH_2";
export const SHELLFISH_3 = "SHELLFISH_3";
export const SHELLFISH_4 = "SHELLFISH_4";
export const SNAIL = "SNAIL";
export const SNAKE_1 = "SNAKE_1";
export const SNAKE_COBRA = "SNAKE_COBRA";
export const SPIDER_1 = "SPIDER_1";
export const SPIDER_2 = "SPIDER_2";
export const SQUIRREL = "SQUIRREL";
export const STARFISH = "STARFISH";
export const TURTLE_1 = "TURTLE_1";
export const TURTLE_2 = "TURTLE_2";
export const UNICORN_1 = "UNICORN_1";
export const UNICORN_HEAD = "UNICORN_HEAD";
export const WOLF = "WOLF";
export const SCORPION = "SCORPION";
export const BONE = "BONE";
export const FISHBOWL = "FISHBOWL";
export const PAW_PRINTS_2 = "PAW_PRINTS_2";
export const PAW_PRINTS_3 = "PAW_PRINTS_3";
export const SPIDERWEB = "SPIDERWEB";
export const UNKNOWN = "UNKNOWN";

export const ALL_PLAYER_ICONS =[
    ANT_ICON, BEAR_1, BEAR_ICON, BEAR_2, BIRD_1, BIRD_SMALL, BIRD_OF_PREY, BIRD_EAGLE_ATTACK, BIRD_EAGLE, BIRD_DOVE_ICON, BIRD_DUCK_FLIGHT, BIRD_HERON, BIRD_SMALL_ICON, BULL, BULL_ICON, BUTTERFLY_1, BUTTERFLY_2, BUTTERFLY_3, CAMEL, DROMEDARY, CAT_PAW, CAT_SIT, CAT_ICON_CUTE_1, CAT_ICON_CUTE_2, CAT_ICON_CUTE_3, CAT_ICON_CUTE_4, CAT_ICON_CUTE_5, CAT_ICON_CUTE_6, CAT_PLAY, CAT_ICON, CAT_KITTEN, CAT_ICON_CUTE_7, CAT_ICON_CUTE_8, COW, COW_ICON, CRAB, CROCODILE, DEER_FEMALE, DEER_JUMP, DEER_MALE, DINO_T_REX, DINO_BRONTO, DINO_TRICERATOPS, DINE_STEGGO, DOG_PAW, DOG_ICON_LEASH, DOG_STAND, DOG_PUPPY, DOG_ICON_CUTE, DOG_2, DRAGON_1, DRAGON_2, DRAGON_3, DRAGON_4, DRAGON_5, DRAGONFLY, DUCK, DUCK_ICON, ELEPHANT_FRONT, ELEPHANT_RIGHT, ELEPHANT_LEFT, FISH_1, FISH_2, FISH_3, FISH_STINGRAY, FISH_SHARK, FROG, GIRAFFE, GIRAFFE_ICON, HAMSTER_ICON, RHINO_RIGHT, RHINO_LEFT, HORSE_1, HORSE_2, KANGAROO, KOALA, LADYBUG_1, LADYBUG_2, LADYBUG_ICON, LADYBUG_3, LADYBUG_4, LADYBUG_5, LION_1, LION_BANNER, LION_2, LION_3, LIZARD_1, LIZARD_2, LOBSTER_1, LOBSTER_2, MONKEY_1, MONKEY_2, MOOSE, MOUSE, PANDA_1, PANDA_2, PAW_PRINTS_1, PENGUIN, RABBIT, RABBIT_ICON, SEAHORSE_1, SEAHORSE_2, SEAL, SHELLFISH_1, SHELLFISH_2, SHELLFISH_3, SHELLFISH_4, SNAIL, SNAKE_1, SNAKE_COBRA, SPIDER_1, SPIDER_2, SQUIRREL, STARFISH, TURTLE_1, TURTLE_2, UNICORN_1, UNICORN_HEAD, WOLF, SCORPION, BONE, FISHBOWL, PAW_PRINTS_2, PAW_PRINTS_3, SPIDERWEB];

export const playerIconMap: Record<string, string> = {

    ANT_ICON: "animal-antz.png",
    BEAR_1: "animal-bear.png",
    BEAR_ICON: "animal-bear3.png",
    BEAR_2: "animal-bear4-sc44.png",
    BIRD_1: "animal-bird.png",
    BIRD_SMALL: "animal-bird11-sc48.png",
    BIRD_OF_PREY: "animal-bird2.png",
    BIRD_EAGLE_ATTACK: "animal-bird4-sc44.png",
    BIRD_EAGLE: "animal-bird5-sc44.png",
    BIRD_DOVE_ICON: "animal-bird6-sc44.png",
    BIRD_DUCK_FLIGHT: "animal-bird7-sc44.png",
    BIRD_HERON: "animal-bird8-sc45.png",
    BIRD_SMALL_ICON: "animal-birdie1.png",
    BULL: "animal-bull-face.png",
    BULL_ICON: "animal-bull1-sc44.png",
    BUTTERFLY_1: "animal-butterfly2.png",
    BUTTERFLY_2: "animal-butterfly3.png",
    BUTTERFLY_3: "animal-butterfly5-sc48.png",
    CAMEL: "animal-camel.png",
    DROMEDARY: "animal-camel2-sc36.png",
    CAT_PAW: "animal-cat-print.png",
    CAT_SIT: "animal-cat1.png",
    CAT_ICON_CUTE_1: "animal-cat18.png",
    CAT_ICON_CUTE_2: "animal-cat20.png",
    CAT_ICON_CUTE_3: "animal-cat21.png",
    CAT_ICON_CUTE_4: "animal-cat26.png",
    CAT_ICON_CUTE_5: "animal-cat27.png",
    CAT_ICON_CUTE_6: "animal-cat28.png",
    CAT_PLAY: "animal-cat3.png",
    CAT_ICON: "animal-cat4.png",
    CAT_KITTEN: "animal-cat5-sc22.png",
    CAT_ICON_CUTE_7: "animal-cat6.png",
    CAT_ICON_CUTE_8: "animal-cat7.png",
    COW: "animal-cow1.png",
    COW_ICON: "animal-cow2.png",
    CRAB: "animal-crab2.png",
    CROCODILE: "animal-crocodile-sc43.png",
    DEER_FEMALE: "animal-deer1.png",
    DEER_JUMP: "animal-deer2.png",
    DEER_MALE: "animal-deer4-sc44.png",
    DINO_T_REX: "animal-dinosaur1.png",
    DINO_BRONTO: "animal-dinosaur2.png",
    DINO_TRICERATOPS: "animal-dinosaur3.png",
    DINE_STEGGO: "animal-dinosaur4.png",
    DOG_PAW: "animal-dog-print.png",
    DOG_ICON_LEASH: "animal-dog1.png",
    DOG_STAND: "animal-dog2.png",
    DOG_PUPPY: "animal-dog3.png",
    DOG_ICON_CUTE: "animal-dog4.png",
    DOG_2: "animal-dog5-sc44.png",
    DRAGON_1: "animal-dragon1.png",
    DRAGON_2: "animal-dragon2.png",
    DRAGON_3: "animal-dragon3-sc28.png",
    DRAGON_4: "animal-dragon4-sc28.png",
    DRAGON_5: "animal-dragon5-sc28.png",
    DRAGONFLY: "animal-dragonfly1-sc43.png",
    DUCK: "animal-duck2-sc43.png",
    DUCK_ICON: "animal-duck4.png",
    ELEPHANT_FRONT: "animal-elephant.png",
    ELEPHANT_RIGHT: "animal-elephant1.png",
    ELEPHANT_LEFT: "animal-elephant5-sc43.png",
    FISH_1: "animal-fish.png",
    FISH_2: "animal-fish1.png",
    FISH_3: "animal-fish13.png",
    FISH_STINGRAY: "animal-fish6.png",
    FISH_SHARK: "animal-fish7-sc37.png",
    FROG: "animal-frog.png",
    GIRAFFE: "animal-giraffe.png",
    GIRAFFE_ICON: "animal-giraffe1.png",
    HAMSTER_ICON: "animal-hamster.png",
    RHINO_RIGHT: "animal-hippo.png",
    RHINO_LEFT: "animal-hippo3-sc22.png",
    HORSE_1: "animal-horse1.png",
    HORSE_2: "animal-horse3-sc44.png",
    KANGAROO: "animal-kangaroo-sc36.png",
    KOALA: "animal-koala-bear.png",
    LADYBUG_1: "animal-ladybug4-sc24.png",
    LADYBUG_2: "animal-ladybug5-sc24.png",
    LADYBUG_ICON: "animal-ladybug6-sc24.png",
    LADYBUG_3: "animal-ladybug7-sc24.png",
    LADYBUG_4: "animal-ladybug8-sc24.png",
    LADYBUG_5: "animal-ladybug9-sc24.png",
    LION_1: "animal-lion.png",
    LION_BANNER: "animal-lion1-sc36.png",
    LION_2: "animal-lion2.png",
    LION_3: "animal-lion3-sc37.png",
    LIZARD_1: "animal-lizard1.png",
    LIZARD_2: "animal-lizard2-sc37.png",
    LOBSTER_1: "animal-lobster.png",
    LOBSTER_2: "animal-lobster1-sc44.png",
    MONKEY_1: "animal-monkey.png",
    MONKEY_2: "animal-monkey1.png",
    MOOSE: "animal-moose-sc44.png",
    MOUSE: "animal-mouse1.png",
    PANDA_1: "animal-panda.png",
    PANDA_2: "animal-panda3-sc37.png",
    PAW_PRINTS_1: "animal-paw-prints2.png",
    PENGUIN: "animal-penguine-sc43.png",
    RABBIT: "animal-rabbit1.png",
    RABBIT_ICON: "animal-rabbit2-sc25.png",
    SEAHORSE_1: "animal-seahorse1-sc44.png",
    SEAHORSE_2: "animal-seahorse2-sc37.png",
    SEAL: "animal-seal.png",
    SHELLFISH_1: "animal-seashell-sc5.png",
    SHELLFISH_2: "animal-seashell2-sc44.png",
    SHELLFISH_3: "animal-shellfish2-sc44.png",
    SHELLFISH_4: "animal-shellfish3.png",
    SNAIL: "animal-snail.png",
    SNAKE_1: "animal-snake.png",
    SNAKE_COBRA: "animal-snake1.png",
    SPIDER_1: "animal-spider.png",
    SPIDER_2: "animal-spider1.png",
    SQUIRREL: "animal-squirrel1-sc37.png",
    STARFISH: "animal-starfish-sc43.png",
    TURTLE_1: "animal-turtle.png",
    TURTLE_2: "animal-turtle3-sc44.png",
    UNICORN_1: "animal-unicorn.png",
    UNICORN_HEAD: "animal-unicorn1.png",
    WOLF: "animal-wolf-sc44.png",
    SCORPION: "astrology1-scorpion-sc37.png",
    BONE: "bone.png",
    FISHBOWL: "fishbowl.png",
    PAW_PRINTS_2: "paw-prints2.png",
    PAW_PRINTS_3: "paw-print3.png",
    SPIDERWEB: "spiderweb2.png",
    UNKNOWN: "question-mark.png",

};



export const iconToPath = (icon: string): string => {
    const fileName = playerIconMap[icon] ?? "question-mark.png"
    return "/images/frontier/actor/player/" + fileName
};