const themeReducer = (state = "frontier", action) => {
    switch(action.type) {
        default: return state;
    }
};

export default themeReducer
