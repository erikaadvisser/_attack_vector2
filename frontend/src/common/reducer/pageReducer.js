import {NAVIGATE_PAGE} from "../enums/CommonActions";

// state: currentPage

const defaultState = "No page yet.";

export default (state = defaultState, action) => {
    switch(action.type) {
        case NAVIGATE_PAGE : return action.to;
        default: return state;
    }
}
