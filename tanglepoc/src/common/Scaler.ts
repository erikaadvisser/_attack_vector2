import * as PIXI from 'pixi.js'

export type XY = { x: number, y: number }


const scaleFactors: XY = {x: 1, y: 1};

export const getWindowSize: () => XY = () => {
    return {
        x: window.innerWidth,
        y: window.innerHeight
    }
};

export const updateScale = () => {
    const size = getCanvasSize(false);
    scaleFactors.x = size.x / 360;
    scaleFactors.y = size.y / 600;

    // scaleFactors.x = 1;
    // scaleFactors.y = 1;

};

export const getCanvasSize: (alsoUpdateScale?: boolean) => XY = (alsoUpdateScale: boolean = false) => {
    const canvasSize = {x: 0, y: 0};

    const size = getWindowSize();
    size.y = size.y - 34 - 20;// reserve space at top for one HTML div, and for a scrollbar at the bottom.
    const ratio = size.y / size.x;

    if (ratio > 5 / 3) {
        canvasSize.x = size.x;
        canvasSize.y = size.x * (5 / 3);
    } else {
        canvasSize.y = size.y;
        canvasSize.x = size.y / (5 / 3);
    }

    if (alsoUpdateScale) {
        updateScale();
    }

    return canvasSize;
};

export const resize = (renderer: PIXI.Renderer) => {

    const newSize = getCanvasSize();
    renderer.resize(newSize.x, newSize.y);
    updateScale();
    console.log(`Canvas size ${newSize.x} x ${newSize.y}`);
    return newSize;
};


export const setPositionScaled = (position: PIXI.IPoint, realx: number, realy: number) => {
    position.x = realx * scaleFactors.x;
    position.y = realy * scaleFactors.y;
};

export const getUnscaledPosition: (position: PIXI.IPoint) => XY = (position: PIXI.IPoint) => {
    return {
        x: position.x / scaleFactors.x,
        y: position.y / scaleFactors.y
    }
};