import webstomp, {Client, Frame, Message, Subscription} from 'webstomp-client';
import {SERVER_DISCONNECT, SERVER_ERROR, SERVER_FORCE_DISCONNECT, SET_USER_ID} from "./CommonActions";
import {notify_fatal} from "./Notification";
import {Store} from "redux";

class WebSocketConnection {

    client: Client | null = null;
    url: string;
    developmentServer: boolean;
    store: Store | null = null;

    waitingForType: string | null = null;
    waitingIgnoreList: Array<string> = [];

    subscriptions: Record<string, Subscription> = {};

    constructor() {
        let port = window.location.port;
        let hostName = window.location.hostname;
        this.developmentServer = (port === "3000");
        // During development connect directly to backend, websocket proxying does not work with create-react-app.
        port = this.developmentServer ? "80" : port;
        let protocol = (window.location.protocol === "http:") ? "ws" : "wss";
        this.url = protocol + "://" + hostName + ":" + port + "/attack_vector_websocket?erik";

    }


    create(store: Store, additionalOnWsOpen: () => void, waitForType: string | undefined = undefined) {
        this.store = store;
        this.client = webstomp.client(this.url, {debug: false, heartbeat: {incoming: 0, outgoing: 0}});

        if (waitForType) {
            this.waitFor(waitForType, []);
        }

        this.client.connect({}, (event) => {
            this.onWsOpen(event, additionalOnWsOpen);
        }, (event) => {
            this.onWsConnectError(event);
        });
    }

    getClient(): Client {
        return (this.client) as Client
    }

    getStore(): Store {
        return (this.store) as Store
    }

    onWsOpen(event: Frame | undefined, additionalOnWsOpen: () => void) {
        if (event) {

            const userId = event.headers["user-name"];
            if (!userId || userId === "error") {
                notify_fatal("Please close this browser tab, hackers can only use one browser tab at a time..");
                return
            }

            // notify_neutral('Status','Connection with server established (' + userName + ")");
            this.dispatch({type: SET_USER_ID, userId: userId});

            this.setupHeartbeat();
            this.subscribe('/user/reply', false);
            additionalOnWsOpen();
        }
    }


    onWsConnectError(event: CloseEvent | Frame) {
        this.dispatch({type: SERVER_DISCONNECT});
    }

    setupHeartbeat() {
        // don't set up heartbeat for development server, as the server will not disconnect anyway,
        // and heartbeat messages wil clutter other messages.
        if (!this.developmentServer) {
            // Our Server does not support server side heartbeats
            // because we are using Simple Broker, not full blown RabitMQ or the like.
            // so we use this home brew alternative
            setInterval(() => {
                this.getClient().send("/hb", "");
            }, 1000 * 15);
        }
    }


    subscribe(path: string, canUnsubscribe: boolean) {
        const subscription = this.getClient().subscribe(path, (wsMessage) => {
            this.handleEvent(wsMessage);
        });
        if (canUnsubscribe) {
            this.subscriptions[path] = subscription;
        }
    }

    handleEvent(wsMessage: Message) {
        const action = JSON.parse(wsMessage.body);

        if (action.type === SERVER_FORCE_DISCONNECT ||
            action.type === SERVER_ERROR) {
            this.getClient().disconnect();
        }

        if (this.waitingForType && action.type !== SERVER_ERROR) {
            if (action.type === this.waitingForType) {
                this.waitingForType = null;
                this.waitingIgnoreList = [];
            } else {
                if (this.waitingIgnoreList === null || this.waitingIgnoreList.includes(action.type)) {
                    return
                }
            }
        }

        this.getStore().dispatch(action);

        /* Our server (Spring simple broker) does not support ACK , so we keep our network logs clean */
        // wsMessage.ack();
    };

    dispatch(body: any) {
        let event = {...body, globalState: this.getStore().getState()};
        this.getStore().dispatch(event);
    }

    subscribeForRun(runId: string, siteId: string) {
        this.subscribe('/topic/run/' + runId, true);
        this.subscribe('/topic/site/' + siteId, true);
    }

    subscribeForPuzzle(puzzleId: string) {
        this.subscribe('/topic/puzzle/' + puzzleId, true)
    }

    send(path: string, data: string | object) {
        const body: string = (typeof data === "object") ? JSON.stringify(data) : data;
        const serverPath = "/av/" + path;
        this.getClient().send(serverPath, body);
    }

    /** Ignore certain actions until an action with a specific type is received.
     * For example: make sure we get the init scan event before we start parsing changes to the site.
     * if ignoreList is null, then all events are ignored that are not the specified type.*/
    waitFor(type: string, ignoreList: Array<string>) {
        this.waitingForType = type;
        this.waitingIgnoreList = ignoreList;
    }

    unsubscribeAll() {
        Object.values(this.subscriptions).forEach(subscription => {
            subscription.unsubscribe();
        });
        this.subscriptions = {};
    }

    unsubscribe(path: string) {
        const subscription = this.subscriptions[path];
        delete this.subscriptions[path];
        subscription.unsubscribe();
    }

    unsubscribeFromPuzzle(puzzleId: string) {
        this.unsubscribe('/topic/puzzle/' + puzzleId)
    }

    abort() {
        this.getClient().disconnect();
    }
}

const webSocketConnection = new WebSocketConnection();

export default webSocketConnection;
