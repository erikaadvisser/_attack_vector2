import {AnyAction} from "redux";

const defaultState = {
    canvasWidth: 200
};

export const CANVAS_RESIZE = "CANVAS_RESIZE";

export interface UiState {
    canvasWidth: number;
}

export const uiReducer = (state: UiState = defaultState, action: AnyAction): UiState => {

    switch (action.type) {
        case CANVAS_RESIZE:
            return { canvasWidth: action.width };
        default:
            return state;
    }

};