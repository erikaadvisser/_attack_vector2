import {combineReducers} from 'redux'
import {tanglePuzzleReducer} from "./tangle/TangleReducer";
import {uiReducer, UiState} from "./common/UiReducer";
import {TanglePuzzle} from "./tangle/TanglePuzzleModel";
import playersReducer, {Players} from "./player/PlayersReducer";



export interface RootState {
    puzzle: TanglePuzzle,
    ui: UiState,
    players: Players,
}

const rootReducer = combineReducers({
    puzzle: tanglePuzzleReducer,
    ui: uiReducer,
    players: playersReducer
});

export default rootReducer;