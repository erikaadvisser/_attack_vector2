import React from 'react';
import rootReducer from "./RootReducer";
import {applyMiddleware, compose, createStore} from "redux";
import {Provider} from "react-redux";
import {render} from "react-dom";
import App from "./App";
import webSocketConnection from "./common/WebSocketConnection";
import {EffectsMiddleware} from "./common/SideEffects";
import registerEffects from "./tangle/TangleSideEffects";


const path = window.location.pathname;
if (path.length !== 8 || !path.startsWith("/p/p")) {
    alert("Invalid path / URL");
    throw Error("Invalid URL: " + path)
}
const  puzzle_id = "tangle-" + path.substr(4);
const rootElement = document.getElementById('root') as Element;


const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, {}, composeEnhancers(applyMiddleware(EffectsMiddleware)));
registerEffects(store.dispatch, puzzle_id);


render(
    <Provider store={store}>
        <div className="container" id="container">
            <App/>
        </div>
    </Provider>,
    rootElement
);

document.onvisibilitychange = (ev) => {
    const eventType = document.hidden ? "blur" : "focus";
    store.dispatch({type: eventType})
};

// window.onblur = (event: FocusEvent) => {
//     store.dispatch({type: "blur"})
// };
//
// window.onfocus = (event: FocusEvent) => {
//     store.dispatch({type: "focus"})
// };

setTimeout(() => {
    webSocketConnection.create(store, () => {
        store.dispatch({type: "START", puzzle_id: puzzle_id});
        webSocketConnection.subscribeForPuzzle(puzzle_id);
    });
}, 25);

// window.addEventListener('wheel', e => {
//     e.preventDefault();
// }, { passive: false })
//

// const app = new PIXI.Application({
//     // resizeTo: window,        // default: 600
//     backgroundColor: 0x4488ee,
//     antialias: true,    // default: false
//     transparent: false, // default: false
//     resolution: 1       // default: 1
// });
// //
// // // The application will create a canvas element for you that you
// // // can then insert into the DOM.
// document.body.appendChild(app.view);
// new PIXI.Loader().add('bunny', '/images/animals.png').load((loader, resources) => {
//
//     if (resources.bunny) {
//
//         // This creates a texture from a 'bunny.png' image.
//         const bunny = new PIXI.Sprite(resources.bunny.texture);
//
//         // Setup the position of the bunny
//         bunny.x = app.renderer.width / 2;
//         bunny.y = app.renderer.height / 2;
//
//         // Rotate around the center
//         bunny.anchor.x = 0.5;
//         bunny.anchor.y = 0.5;
//
//         let line = new PIXI.Graphics();
//         line.lineStyle(4, 0xFFFFFF, 1);
//         line.moveTo(0, 0);
//         line.lineTo(80, 50);
//         line.x = 32;
//         line.y = 32;
//         app.stage.addChild(line);
//         // Add the bunny to the scene we are building.
//         app.stage.addChild(bunny);
//
//         // Listen for frame updates
//         app.ticker.add(() => {
//             // each frame we spin the bunny around a bit
//             bunny.rotation += 0.01;
//         });
//     }
//
// });
