import React from "react";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {TanglePuzzle} from "./TanglePuzzleModel";
import PlayerIcon from "../player/PlayerIcon";
import {RootState} from "../RootReducer";
import {Player, Players} from "../player/PlayersReducer";


const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        restartPuzzle: () => {
            dispatch({type: "TANGLE_RESTART"});
        }
    }
};

let mapStateToProps = (state: RootState) => {
    return {
        puzzle: state.puzzle,
        canvasWidth: state.ui.canvasWidth,
        players: state.players
    };
};

interface Props {
    puzzle: TanglePuzzle,
    canvasWidth: number
    restartPuzzle: () => void,
    players: Players
}

const restartButton = (puzzle: TanglePuzzle, restartPuzzle: () => void) => {
    if (puzzle.solved) {
        return <>
            <button className="btn btn-primary" onClick={restartPuzzle}>New Puzzle</button>
            &nbsp;</>
    }

    return <button className="btn btn-primary hidden_alpha">-</button>
};

const renderPlayer = (player: Player) => {
    return <PlayerIcon player={player}/>

};

const component: React.FC<Props> = ({puzzle, canvasWidth, restartPuzzle, players}) => {
    if (!puzzle) {
        return <div style={{marginBlockStart: 0, marginBlockEnd: 0}} id="headerDiv">loading</div>
    }
    return <div style={{marginBlockStart: 0, marginBlockEnd: 0, width: canvasWidth}} id="headerDiv">
        {restartButton(puzzle, restartPuzzle)}
        <span className="text">Dificulty: {puzzle.strength}</span>
        {players.map(renderPlayer)}
    </div>
};

export default connect(mapStateToProps, mapDispatchToProps)(component);
