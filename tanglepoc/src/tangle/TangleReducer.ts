import {SERVER_START_TANGLE_STANDALONE, SERVER_TANGLE_SOLVED} from "./TangleIceActions";


export const tanglePuzzleReducer =  (state = {}, action: any ) => {
    switch (action.type) {
        case SERVER_START_TANGLE_STANDALONE:
            return action.data.puzzle;
        case SERVER_TANGLE_SOLVED:
            return { ...state, solved: true };
        default: return state;
    }
};
