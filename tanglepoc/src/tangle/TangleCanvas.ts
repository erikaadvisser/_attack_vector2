import * as PIXI from 'pixi.js'
import TanglePointDisplay from "./TanglePointDisplay";
import TangleLineDisplay from "./TangleLineDisplay";
import {TanglePointMoved, TanglePuzzleStart} from "./TanglePuzzleModel";
import {Dispatch} from "redux";
import {getCanvasSize, resize} from "../common/Scaler";
import {CANVAS_RESIZE} from "../common/UiReducer";


class TangleCanvas {

    app: PIXI.Application | null = null;
    pointsById: Record<string, TanglePointDisplay> = {};


    resize() {
        resize(this.app!.renderer);
        // document.getElementById("infotext")!.innerText = "w: " + newSize.x + ", h: " + newSize.y;
    }


    init(dispatch: Dispatch, data: TanglePuzzleStart) {
        const canvasSize = getCanvasSize(true);
        console.log(`Canvas size ${canvasSize.x} x ${canvasSize.y}`);

        // document.getElementById("infotext")!.innerText = "w: " + canvasSize.x + ", h: " + canvasSize.y;
        const canvasElement = document.getElementById("pixiCanvas") as HTMLCanvasElement;

        // alert('construct2, canvasElement = ' + canvasElement);
        const app = new PIXI.Application({
            width: canvasSize.x,
            height: canvasSize.y,
            // resizeTo: canvasElement,        // default: 600
            backgroundColor: 0x333333,
            antialias: true,    // default: false
            transparent: false, // default: false
            resolution: 1,       // default: 1
            view: canvasElement
        });
        this.app = app;

        this.app.stage.sortableChildren = true;
        // console.log("system started: " + PIXI.Ticker.system.started + ", fps: " + PIXI.Ticker.system.FPS);

        dispatch({type: CANVAS_RESIZE, width: canvasSize.x, height: canvasSize.y});



        data.puzzle.points.forEach(point => {
            const pointDisplay = new TanglePointDisplay(dispatch, app.stage, point, data.puzzle.strength);
            this.pointsById[point.id] = pointDisplay;
        });

        data.puzzle.lines.forEach(line => {
            const fromPointDisplay = this.pointsById[line.fromId];
            const toPointDisplay = this.pointsById[line.toId];
            const lineDisplay = new TangleLineDisplay(app.stage, line.id, fromPointDisplay, toPointDisplay, data.puzzle.strength);

            fromPointDisplay.addLine(lineDisplay);
            toPointDisplay.addLine(lineDisplay);
        });

        // new TanglePointDisplay(dispatch, app.stage, {x: 360, y: 640, id: '00'}, data.puzzle.strength);
        // new TanglePointDisplay(dispatch, app.stage, {x: 360 * 1.5, y: 640 * 1.5, id: '00'}, data.puzzle.strength);

        // console.log("system started: " + PIXI.Ticker.system.started + ", fps: " + PIXI.Ticker.system.FPS);

    }


    moved(data: TanglePointMoved) {
        this.pointsById[data.id].moved(data.x, data.y);
    }
}


const tangleCanvas = new TangleCanvas();

export {
    tangleCanvas
};


// const d = document.getElementById("headerDiv")!;
// d.parentNode!.removeChild(d);


//         new PIXI.Loader().add('bunny', '/images/animals.png').load((loader, resources) => {
//
//             if (resources.bunny) {
//
//                 // This creates a texture from a 'bunny.png' image.
//                 const bunny = new PIXI.Sprite(resources.bunny.texture);
//
//                 // Setup the position of the bunny
//                 bunny.x = this.app!.renderer.width / 2;
//                 bunny.y = this.app!.renderer.height / 2;
//
//                 // Rotate around the center
//                 bunny.anchor.x = 0.5;
//                 bunny.anchor.y = 0.5;
//
//
//                 const fragmentFilter = `
// precision mediump float;
//
// varying vec2 vTextureCoord;
// uniform sampler2D uSampler;
// uniform float uInput;
// void main(void){
//    gl_FragColor = texture2D(uSampler, vTextureCoord);
//    gl_FragColor.r = uInput;
// }
// `;
//
//                 const uniforms = {uInput: 1.0};
//
//
//                 const filter = new PIXI.Filter("", fragmentFilter, uniforms);
//
//                 bunny.filters = [filter];
//
//
//                 this.app!.stage.addChild(bunny);
//                 this.app!.ticker.add(() => {
//                     // each frame we spin the bunny around a bit
//                     bunny.rotation += 0.01;
//                 });
//             }
//
//
//
//         });