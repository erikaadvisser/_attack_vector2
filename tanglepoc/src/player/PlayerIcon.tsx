import React from "react";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {Player} from "./PlayersReducer";
import {iconToPath} from "./PlayerIconMap";


const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
    }
};

let mapStateToProps = (state: any) => {
    return {
    };
};

interface Props {
    player: Player,
}


const component: React.FC<Props> = ({player}) => {
    const source = iconToPath(player.icon);
    return <img src={source} width="34" height="34" style={{float:"right"}} alt=""/>
};

export default connect(mapStateToProps, mapDispatchToProps)(component);
