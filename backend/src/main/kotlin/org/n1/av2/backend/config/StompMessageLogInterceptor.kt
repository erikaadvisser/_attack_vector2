package org.n1.av2.backend.config

import mu.KLogger
import mu.KLogging
import org.n1.av2.backend.model.db.ActionLog
import org.n1.av2.backend.model.iam.SessionPrincipal
import org.n1.av2.backend.repo.ActionLogRepo
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.support.ChannelInterceptor
import java.nio.charset.StandardCharsets.UTF_8
import java.time.LocalDateTime
import java.util.*


class MessageIn(val actionLogRepo: ActionLogRepo) : ChannelInterceptor {
    companion object : KLogging()

    override fun preSend(message: Message<*>, channel: MessageChannel): Message<*> {
        if (message.headers["stompCommand"] == null) {
            return message
        }

        val command = message.headers["stompCommand"] as StompCommand
        if ("SEND" == command.name) {
            log(logger, message, "in >")

        }
        return message
    }

    private fun log(logger: KLogger, message: Message<*>, directionText: String) {
        val payload = message.payload as ByteArray
        val payloadString = String(payload, UTF_8)
        val destination = message.headers["simpDestination"].toString()

        val connectionSummary = if (message.headers["simpUser"] != null) {
            (message.headers["simpUser"] as SessionPrincipal).name
//            (message.headers["simpUser"] as SessionPrincipal).name.substring(0, 12)
        } else {
            "no name"
        }
        logger.debug("${directionText} [${connectionSummary}] ${destination} ${payloadString}")

        val log = ActionLog(UUID.randomUUID().toString(), connectionSummary, LocalDateTime.now(), destination, payloadString)
        actionLogRepo.save(log);
    }
}

//class MessageOut : ChannelInterceptor {
//    companion object : KLogging()
//
//    override fun preSend(message: Message<*>, channel: MessageChannel): Message<*>? {
//        log(MessageIn.logger, message, "<out")
//        return super.preSend(message, channel)
//    }
//}

