package org.n1.av2.backend.repo

import org.n1.av2.backend.model.db.ActionLog
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface ActionLogRepo : PagingAndSortingRepository<ActionLog, String> {
}