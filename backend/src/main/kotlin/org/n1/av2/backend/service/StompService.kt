package org.n1.av2.backend.service

import mu.KLogging
import org.n1.av2.backend.model.ui.NotyMessage
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.model.ui.ReduxEvent
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.stereotype.Service

@Service
class StompService(
        val stompTemplate: SimpMessageSendingOperations,
        val sessionService: SessionService) {

    companion object : KLogging()

    @Value("\${ENVIRONMENT ?: default}")
    lateinit var environment: String

    val simulateNonLocalhost = {
        if (environment.startsWith("dev")) {
            Thread.sleep(70)
        }
    }

    private fun toDestination(route: String, actionType: ReduxActions, data: Any? = null) {
        simulateNonLocalhost()
        logger.debug("<out ${route} ${actionType}")
        val event = ReduxEvent(actionType, data)
        stompTemplate.convertAndSend(route, event)
    }

    // --- Site

    fun toSite(siteId: String, actionType: ReduxActions, data: Any? = null) {
        toDestination("/topic/site/${siteId}", actionType, data)
    }

    // --- Run

    fun toRun(runId: String, actionType: ReduxActions, data: Any? = null) {
        toDestination("/topic/run/${runId}", actionType, data)
    }

    // --- Puzzle

    fun toPuzzle(puzzleId: String, actionType: ReduxActions, data: Any? = null) {
        toDestination("/topic/puzzle/${puzzleId}", actionType, data)
    }

    // --- connection

    fun toConnection(actionType: ReduxActions, data: Any) {
        val connectionId = sessionService.currentSessionId
        simulateNonLocalhost()
        val connectionSummary = connectionId
//        val connectionSummary = connectionId.substring(0, 12)
        logger.debug("<out [${connectionSummary}] ${actionType}")
        val event = ReduxEvent(actionType, data)
        stompTemplate.convertAndSendToUser(connectionId, "/reply", event)
    }

    fun toConnection(message: NotyMessage) {
        toConnection(ReduxActions.SERVER_NOTIFICATION, message)
    }

    // --- Hacker

    fun toHacker(actionType: ReduxActions, data: Any) {
        val userId = sessionService.currentUserId
        toHacker(userId, actionType, data)
    }


    fun toHacker(userId: String, actionType: ReduxActions, data: Any) {
        simulateNonLocalhost()
        logger.debug("<out ${userId} ${actionType}")
        val event = ReduxEvent(actionType, data)
        stompTemplate.convertAndSendToUser(userId, "/reply", event)
    }

    fun toHacker(userId: String, message: NotyMessage) {
        toHacker(userId, ReduxActions.SERVER_NOTIFICATION, message)
    }

    // --- Terminal

    data class TerminalReceive(val terminalId: String, val lines: Array<out String>)
    fun terminalReceiveCurrentUser(vararg lines: String) {
        toHacker(ReduxActions.SERVER_TERMINAL_RECEIVE, TerminalReceive("main", lines))
    }

    data class TerminalReceiveLinesAsCollection(val terminalId: String, val lines: Collection<String>)
    fun terminalReceiveCurrentUser(lines: Collection<String>) {
        toHacker(ReduxActions.SERVER_TERMINAL_RECEIVE, TerminalReceiveLinesAsCollection("main", lines))
    }

    fun terminalReceiveForUser(userId: String, vararg lines: String) {
        toHacker(userId, ReduxActions.SERVER_TERMINAL_RECEIVE, TerminalReceive("main", lines))
    }

    fun terminalReceiveForUserForTerminal(userId: String, terminalId: String, vararg lines: String) {
        toHacker(userId, ReduxActions.SERVER_TERMINAL_RECEIVE, TerminalReceive(terminalId, lines))
    }

}