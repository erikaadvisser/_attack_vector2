package org.n1.av2.backend.service.user

import org.n1.av2.backend.model.db.user.HackerIcon
import org.n1.av2.backend.model.db.user.User
import org.n1.av2.backend.model.db.user.UserType
import org.n1.av2.backend.repo.UserRepo
import org.n1.av2.backend.util.createId
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.util.*
import javax.annotation.PostConstruct

@Component
class UserService(
        val userRepo: UserRepo,
        val template: MongoTemplate
) {

    val passwordEncoder = BCryptPasswordEncoder(4)

    fun findByName(userName: String): User? {
        return userRepo.findByNameIgnoreCase(userName)
    }

    fun getByName(userName: String): User {
        return findByName(userName) ?: throw UsernameNotFoundException("Invalid username or password.")
    }

    fun login(userName: String, password: String): User {


        val user = getByName(userName)
        if (passwordEncoder.matches(password, user.encodedPasscoded)) {
            return user
        }
        else {
            throw UsernameNotFoundException("Invalid username or password.")
        }
    }

    fun signUpAnonymous(name: String, icon: String): User {
        val newUser = User(
                id = createUserId(false),
                name = name,
                type = UserType.ANONYMOUS,
                icon = HackerIcon.valueOf(icon)
        )
        val randomPassword =  UUID.randomUUID().toString() // just to prevent someone else from ever logging in as this anonymous user
        createUser(newUser, randomPassword)
        return newUser
    }

    fun createUser(userInput: User, password: String): User {
        userInput.encodedPasscoded = passwordEncoder.encode(password )
        userRepo.save(userInput)
        return userInput
    }

    @PostConstruct
    fun createMandatoryUsers() {
        mandatoryUser("admin", "", UserType.ADMIN)
        mandatoryUser("gm", "", UserType.GM)
        mandatoryUser("hacker", "", UserType.USER, HackerIcon.CROCODILE)
        mandatoryUser("h", "", UserType.USER, HackerIcon.KOALA)

        mandatoryUser("Stalker", "", UserType.USER, HackerIcon.BEAR_1)
        mandatoryUser("Obsidian", "", UserType.USER, HackerIcon.BIRD_1)
        mandatoryUser("Paradox", "", UserType.USER, HackerIcon.BULL)
        mandatoryUser("Shade_zero", "", UserType.USER, HackerIcon.BIRD_EAGLE)
        mandatoryUser("_eternity_", "", UserType.USER, HackerIcon.SNAKE_COBRA)
        mandatoryUser("BoltBishop", "", UserType.USER, HackerIcon.DRAGON_1)
        mandatoryUser("CryptoLaw", "", UserType.USER, HackerIcon.FROG)
        mandatoryUser("Moonshine", "", UserType.USER, HackerIcon.LION_1)
        mandatoryUser("Angler", "", UserType.USER, HackerIcon.FISH_SHARK)
        mandatoryUser("N1X", "", UserType.USER, HackerIcon.FISH_STINGRAY)
        mandatoryUser("Face.dread" , "", UserType.USER, HackerIcon.LIZARD_1)
        mandatoryUser("-=Silver=-", "", UserType.USER, HackerIcon.SNAKE_1)
        mandatoryUser("C_H_I_E_F", "", UserType.USER, HackerIcon.WOLF)
        mandatoryUser(".Specter.", "", UserType.USER, HackerIcon.UNICORN_1)
    }

    fun mandatoryUser(userName: String, password: String, type: UserType, icon: HackerIcon = HackerIcon.UNKNOWN) {
        val user = findByName(userName)
        if (user == null) {
            val newUser = User(
                    id = createUserId(false),
                    name = userName,
                    type = type,
                    icon = icon
            )
            createUser(newUser, password)
        }
    }

    fun createUserId(anonymous: Boolean): String {
        val prefix = if (anonymous) "anon" else "user"
        return createId(prefix, userRepo::findById)
    }

    fun purgeAll() {
        userRepo.deleteAll()
        createMandatoryUsers()
    }

    fun getById(userId: String): User {
        return userRepo.findById(userId).orElseGet { error("${userId} not found") }
    }

    fun findAll(): List<User> {
        return userRepo.findAll().toList()
    }
}
