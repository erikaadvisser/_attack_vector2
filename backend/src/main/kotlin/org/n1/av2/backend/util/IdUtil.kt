package org.n1.av2.backend.util

import org.n1.av2.backend.model.db.site.Node
import java.util.*


fun neverFindExisting(ignore: String): Any? {
    return null
}


/**
 * Create a unique ID of the form ${prefix}-12ef . For example: tangle-7a4b
 *
 * The second parameter is a function that finds existing ids. If this function returns null or and Optional with no value,
 * then the id is unique.
 *
 * Usage: createId("tangle", puzzleRepo::findById)
 */
/*
This ID is a compromise between readability and chance of collision. 2 bytes gives about 64K options, so the chance that someone guesses or mistypes
an ID is small. Uniqueness is guaranteed by explicit checking.
 */
fun createId(prefix: String, findExisting: (String)-> Any?): String {
    return createId(prefix, findExisting, 0, 4)
}

fun createLayerId(node: Node, findExisting: (String)-> Any?): String {
    return createId("${node.id}-layer", findExisting, 0, 4)
}

fun nodeIdFromServiceId(layerId: String): String {
    return layerId.substring(0,9)
}

fun createId(prefix: String, findExisting: (String)-> Any?, start: Int, end: Int): String {
    var count = 0
    var id: String
    do {
        val uuidPart = UUID.randomUUID().toString().substring(start, end)
        id = "$prefix-$uuidPart"
        var existing = findExisting(id)
        if (existing is Optional<*> && !existing.isPresent) {
            existing = null
        }
        count ++
    }
    while (existing != null && count < 10)

    if (count == 10) {
        throw RuntimeException("Failed to generate unique id for prefix: ${prefix} ")
    }
    return id
}

