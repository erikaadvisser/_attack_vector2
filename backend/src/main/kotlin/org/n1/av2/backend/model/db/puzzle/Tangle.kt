package org.n1.av2.backend.model.db.puzzle


class TanglePuzzle(
        val id: String,
        var solved: Boolean,
        val type: PuzzleUsageType,
        val owner: String,
        val description: String,

        val strength: PuzzleStrength,
        val points: MutableList<TanglePoint>,
        val lines: List<TangleLine>
)

data class TanglePoint(val id: String, var x: Int, var y: Int)

data class TangleLine(val id: String, val fromId: String, val toId: String)
