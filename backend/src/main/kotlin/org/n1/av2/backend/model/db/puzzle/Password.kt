package org.n1.av2.backend.model.db.puzzle

import java.time.ZonedDateTime

class PasswordPuzzle (
        val id: String,
        var solved: Boolean,
        val type: PuzzleUsageType,
        val owner: String,
        val description: String,

        val password: String,
        val hint: String?,
        val attempts: MutableList<String>,
        var lockedUntil: ZonedDateTime

)