package org.n1.av2.backend.service.layer

import org.n1.av2.backend.model.db.layer.IcePasswordLayer
import org.n1.av2.backend.model.db.layer.IceTangleLayer
import org.n1.av2.backend.model.db.layer.Layer
import org.n1.av2.backend.model.db.site.Node
import org.n1.av2.backend.model.db.site.enums.LayerType
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.layer.ice.password.IcePasswordService
import org.n1.av2.backend.service.layer.ice.tangle.IceTangleService
import org.n1.av2.backend.service.run.LayerStatusService

@org.springframework.stereotype.Service
class ServiceIceGeneric(
        private val layerStatusService: LayerStatusService,
        private val icePasswordService: IcePasswordService,
        private val iceTangleService: IceTangleService,
        private val stompService: StompService) {

    fun hack(node: Node, layer: Layer, runId: String) {
        val layerStatus = layerStatusService.getLayerStatusOrNull(layer.id, runId)
        if (layerStatus != null && layerStatus.hacked ) {
            stompService.terminalReceiveCurrentUser("[info]not required[/] Ice already hacked.")
            return
        }

        when (layer.type) {
            LayerType.ICE_PASSWORD -> icePasswordService.hack(node, layer as IcePasswordLayer, runId)
            LayerType.ICE_TANGLE -> iceTangleService.hack(node, layer as IceTangleLayer, runId)
            else -> error("unsupported hack type: ${layer.type}")
        }
    }




}