package org.n1.av2.backend.model.ui

class ReduxEvent(val type: ReduxActions,
                 val data: Any? = null)
