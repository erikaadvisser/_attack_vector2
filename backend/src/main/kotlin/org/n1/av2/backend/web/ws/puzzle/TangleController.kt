package org.n1.av2.backend.web.ws.puzzle

import org.n1.av2.backend.engine.SerializingExecutor
import org.n1.av2.backend.service.puzzle.TanglePuzzleService
import org.n1.av2.backend.service.puzzle.tangle.TanglePuzzlePlayService
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import java.security.Principal

@Controller
class TangleController(
        val tanglePuzzleService: TanglePuzzleService,
        val tanglePuzzlePlayService: TanglePuzzlePlayService,
        val executor: SerializingExecutor ) {

    data class TanglePointMoveInput(val puzzleId: String,
                                    val pointId: String,
                                    val newX: Int,
                                    val newY: Int)

    @MessageMapping("/puzzle/tangle/moved")
    fun scansOfPlayer(command: TanglePointMoveInput, principal: Principal) {
        executor.run(principal) { tanglePuzzlePlayService.move(command) }
    }

    @MessageMapping("/puzzle/tangle/getAll")
    fun getAllIceTangle(principal: Principal) {
        executor.run(principal) { tanglePuzzleService.findAll() }
    }

    @MessageMapping("/puzzle/tangle/demo")
    fun demo(puzzleId: String, principal: Principal) {
        executor.run(principal) { tanglePuzzleService.demo(puzzleId) }
    }

    @MessageMapping("/puzzle/tangle/demoRestart")
    fun demoRestart(puzzleId: String, principal: Principal) {
        executor.run(principal) { tanglePuzzleService.demoRestart(puzzleId) }
    }

    @MessageMapping("/puzzle/tangle/blur")
    fun blur(puzzleId: String, principal: Principal) {
        println("blur")
    }

    @MessageMapping("/puzzle/tangle/focus")
    fun focus(puzzleId: String, principal: Principal) {
        println("focus")
    }
}