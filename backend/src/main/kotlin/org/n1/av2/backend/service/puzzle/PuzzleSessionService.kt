package org.n1.av2.backend.service.puzzle

import org.n1.av2.backend.model.db.run.PlayerPresenceState
import org.n1.av2.backend.model.db.run.PuzzlePresence
import org.n1.av2.backend.model.db.user.UserSession
import org.n1.av2.backend.model.ui.NotyMessage
import org.n1.av2.backend.model.ui.NotyType
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.service.StompService
import org.springframework.stereotype.Service

@Service
class PuzzleSessionService(
        private val stompService: StompService
) {

    private val tangles = HashMap<String, PuzzlePresence>()

    fun connect(userSession: UserSession, puzzleId: String) {
        val presence = tangles.getOrPut(puzzleId, { PuzzlePresence() })
        if (!presence.players.containsKey(userSession)) {
            // new connecting player
            presence.players[userSession] = PlayerPresenceState.ACTIVE

            stompService.toPuzzle(puzzleId, ReduxActions.SERVER_NOTIFICATION, NotyMessage(NotyType.OK, "Player", userSession.name))
        } else {
            stompService.toPuzzle(puzzleId, ReduxActions.SERVER_NOTIFICATION, NotyMessage(NotyType.OK, "Player 2 times: ", userSession.name))
            // player connecting a second time
        }
    }

    fun forPuzzle(puzzleId: String): PuzzlePresence {
        return tangles.getOrPut(puzzleId, { PuzzlePresence() })
    }
}