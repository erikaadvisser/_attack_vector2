package org.n1.av2.backend.service

import org.n1.av2.backend.model.db.user.User
import org.springframework.stereotype.Service
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse



@Service
class CookiesService () {

   fun addCookies(user: User, jwt: String, response: HttpServletResponse, expiryInS: Int) {
        addJwtCookie(jwt, response, expiryInS)
        addUserNameCookie(user, response, expiryInS)
        addIconCookie(user, response, expiryInS)
        addTypeCookie(user, response, expiryInS)
        addRoleCookie(user, response, expiryInS)
    }


    private fun addUserNameCookie(user: User, response: HttpServletResponse, expiryInS: Int) {
        addCookie("userName", user.name, response, expiryInS)
    }

    private fun addIconCookie(user: User, response: HttpServletResponse, expiryInS: Int) {
        addCookie("icon", user.icon.toString(), response, expiryInS)
    }

    private fun addJwtCookie(jwt: String, response: HttpServletResponse, expiryInS: Int) {
        addCookie("jwt", jwt, response, expiryInS, true)
    }

    private fun addTypeCookie(user: User, response: HttpServletResponse, expiryInS: Int) {
        val type = user.type.toString()
        addCookie("type", type, response, expiryInS)
    }

    private fun addRoleCookie(user: User, response: HttpServletResponse, expiryInS: Int) {
        val roles = user.type.authorities.joinToString(separator = "|") { it.authority }
        addCookie("roles", roles, response, expiryInS)
    }

    private fun addCookie(name: String, value: String, response: HttpServletResponse, expiryInS: Int) {
        addCookie(name, value, response, expiryInS, false)
    }

    private fun addCookie(name: String, value: String, response: HttpServletResponse, expiryInS: Int, httpOnly: Boolean) {
        val cookie = Cookie(name, value)
        cookie.path = "/"
        cookie.maxAge = expiryInS
//        cookie.setHttpOnly(httpOnly)
        response.addCookie(cookie)
    }

    fun removeCookies(response: HttpServletResponse) {
        removeCookie("userName", response)
        removeCookie("jwt", response)
        removeCookie("type", response)
        removeCookie("roles", response)

    }

    private fun removeCookie(name: String, response: HttpServletResponse) {
        val cookie = Cookie(name, "")
        cookie.path = "/"
        cookie.maxAge = 0
        response.addCookie(cookie)
    }
}