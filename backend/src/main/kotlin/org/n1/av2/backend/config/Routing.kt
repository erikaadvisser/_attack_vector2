package org.n1.av2.backend.config

import org.n1.av2.backend.model.db.user.UserType
import org.n1.av2.backend.model.iam.SessionPrincipal


enum class SessionType {
    MINI_GAME,
    HACKER,
    GM_ADMIN
}

val GM_ADMIN_TYPES = listOf(UserType.GM, UserType.ADMIN)


fun determineSessionType(path: String, principal: SessionPrincipal): SessionType {
    if (path.startsWith("/p/")) {
        return SessionType.MINI_GAME
    }

    if (principal.user.type == UserType.USER) {
        return SessionType.HACKER
    }

    if (GM_ADMIN_TYPES.contains(principal.user.type)) {
        return SessionType.GM_ADMIN
    }

    error("User type: ${principal.user.type} is not compatible with path: ${path}")
}