package org.n1.av2.backend.service.puzzle

import org.n1.av2.backend.model.db.layer.IcePasswordLayer
import org.n1.av2.backend.model.db.puzzle.PasswordPuzzle
import org.n1.av2.backend.model.db.puzzle.PuzzleUsageType
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.repo.PasswordPuzzleRepo
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.TimeService
import org.n1.av2.backend.service.layer.HackedUtil
import org.n1.av2.backend.util.createId
import org.n1.av2.backend.web.ws.puzzle.IcePasswordController
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class PasswordPuzzleService(
        val puzzleRepo: PasswordPuzzleRepo,
        val timeService: TimeService,
        val hackedUtil: HackedUtil,
        val stompService: StompService) {

    fun createPasswordPuzzle(layer: IcePasswordLayer, type: PuzzleUsageType, owner: String, description: String): PasswordPuzzle {
        val id = createId("password", puzzleRepo::findById)
        val puzzle = PasswordPuzzle(
                id = id,
                solved = false,
                type = type,
                owner = owner,
                description = description,
                password = layer.password,
                hint = layer.hint,
                attempts = LinkedList(),
                lockedUntil = timeService.now().minusDays(1)
        )
        puzzleRepo.save(puzzle)
        return puzzle
    }

    fun getPuzzle(puzzleId: String): PasswordPuzzle {
        return puzzleRepo.findByIdOrNull(puzzleId) ?: error("Puzzle not found: ${puzzleId}")
    }

    data class UiState(val puzzle: PasswordPuzzle, val message: String?)

    fun submitAttempt(command: IcePasswordController.SubmitPassword) {
        val puzzle = getPuzzle(command.puzzleId)

        when {
            puzzle.attempts.contains(command.password) -> resolveDuplicate(command.puzzleId, command.password)
            command.password == puzzle.password -> resolveHacked(puzzle, command.password)
            else -> resolveFailed(puzzle, command.password)
        }
    }

    data class UIMessage(val message: String)

    private fun resolveDuplicate(puzzleId: String, password: String) {
        val result = UIMessage("Password \"${password}\" already attempted, ignoring")
        stompService.toPuzzle(puzzleId, ReduxActions.SERVER_ICE_PASSWORD_MESSAGE, result)
    }

    private fun resolveHacked(puzzle: PasswordPuzzle, passwordAttempt: String) {
        puzzle.attempts.add(passwordAttempt)
        puzzle.attempts.sort()
        puzzle.solved = true
        puzzleRepo.save(puzzle)

        val message = UIMessage("Password accepted.")
        stompService.toPuzzle(puzzle.id, ReduxActions.SERVER_ICE_PASSWORD_UPDATE, puzzle)
        stompService.toPuzzle(puzzle.id, ReduxActions.SERVER_ICE_PASSWORD_MESSAGE, message)

        if (puzzle.type == PuzzleUsageType.ICE) {
            hackedUtil.iceHacked(puzzle.id, 70)
        }
    }

    private fun resolveFailed(puzzle: PasswordPuzzle, passwordAttempt: String) {
        puzzle.attempts.add(passwordAttempt)
        puzzle.attempts.sort()
        val timeOutSeconds = calculateTimeOutSeconds(puzzle.attempts.size)
        puzzle.lockedUntil = timeService.now().plusSeconds(timeOutSeconds)

        puzzleRepo.save(puzzle)

        val message = UIMessage("Password incorrect: ${passwordAttempt}")
        stompService.toPuzzle(puzzle.id, ReduxActions.SERVER_ICE_PASSWORD_UPDATE, puzzle)
        stompService.toPuzzle(puzzle.id, ReduxActions.SERVER_ICE_PASSWORD_MESSAGE, message)
    }

    private fun calculateTimeOutSeconds(totalAttempts: Int): Long {
        return when (totalAttempts) {
            0, 1, 2 -> 2L
            3, 4, 5 -> 5L
            6, 7, 8, 9, 10 -> 10L
            else -> 15L
        }
    }

    fun deleteAll() {
        puzzleRepo.deleteAll()
    }


}