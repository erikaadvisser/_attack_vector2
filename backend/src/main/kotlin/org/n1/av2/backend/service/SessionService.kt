package org.n1.av2.backend.service

import org.n1.av2.backend.engine.SYSTEM_USER_ID
import org.n1.av2.backend.model.db.user.UserSession
import org.springframework.stereotype.Service

@Service
class SessionService {
    private val userStore = ThreadLocal<UserSession>()

    fun set(user: UserSession) {
        userStore.set(user)
    }

    val currentUser: UserSession
        get () {
            return userStore.get()
        }

    val currentSessionId: String
        get() {
            return userStore.get().sessionId
        }

    val currentUserId: String
        get () {
            val userId = userStore.get().id
            if (userId == SYSTEM_USER_ID) error("Tried to access the current user from a game event context.")
            return userId
        }

    val isSystemUser: Boolean
        get() {
            val user = userStore.get() ?: return true
            return user.id == SYSTEM_USER_ID
        }

    fun remove() {
        userStore.remove()
    }


}