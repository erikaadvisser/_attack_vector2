package org.n1.av2.backend.model.db.puzzle

enum class PuzzleStrength {

    VERY_WEAK,
    WEAK,
    AVERAGE,
    STRONG,
    VERY_STRONG,
    IMPENETRABLE,
    UNKNOWN,            // Use this to convey to players that they don't know the strength. Don't use this for actual strength of a puzzle.
    NA                  // Use this for puzzles that don't have a strength.
}

enum class Difficulty {

    TUTORIAL,           //
    SIMPLE,             // An experienced player takes 1 minute to solve this
    MODERATE,           // An experience player should take 3-5 minutes for this.
    HARD,               // 5-10 minutes for experienced player
    VERY_HARD,          // 10-20 minutes for experienced player
    EXTREME,


}