package org.n1.av2.backend.model.db.user

import org.n1.av2.backend.model.iam.SessionPrincipal
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
open class User(
        @Id val id: String,
        var failedLoginCount: Int = 0,
        var blockedUntil: Long = 0,
        var name: String = "",
        var encodedPasscoded: String = "",
        var type: UserType = UserType.NOT_LOGGED_IN,
        var icon: HackerIcon)


class UserSession(user: User, val sessionId: String) :
        User(user.id, user.failedLoginCount, user.blockedUntil, user.name, user.encodedPasscoded, user.type, user.icon) {

    constructor(principal: SessionPrincipal) : this(principal.user, principal.name)
}


