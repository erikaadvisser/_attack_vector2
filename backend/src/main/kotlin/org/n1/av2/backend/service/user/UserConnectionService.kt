package org.n1.av2.backend.service.user

import mu.KLogging
import org.n1.av2.backend.engine.TimedEventQueue
import org.n1.av2.backend.model.db.run.HackerGeneralActivity
import org.n1.av2.backend.model.db.user.UserType
import org.n1.av2.backend.model.iam.SessionPrincipal
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.service.SessionService
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.patroller.TracingPatrollerService
import org.n1.av2.backend.service.run.HackerStateService
import org.springframework.stereotype.Service
import java.time.ZonedDateTime


//val runActivities = listOf(HackerActivityType.SCANNING, HackerActivityType.HACKING)

@Service
class UserConnectionService(
        private val hackerStateService: HackerStateService,
        private val sessionService: SessionService,
        private val tracingPatrollerService: TracingPatrollerService,
        private val timedEventQueue: TimedEventQueue,
        private val stompService: StompService) {

    companion object: KLogging()

    /** Returns validity of connection. False means this is a duplicate connection */
    fun connect(sessionPrincipal: SessionPrincipal): Boolean {
        //FIXME:
        if (sessionPrincipal.user.id == "anonymous") {
            return true
        }
        val existingState = hackerStateService.retrieve(sessionPrincipal.userId)
        return if (existingState.generalActivity == HackerGeneralActivity.OFFLINE) {
            hackerStateService.login(sessionPrincipal.userId)
            true
        }
        else {
            logger.info("Rejected duplicate session from: " + sessionPrincipal.user.name )
            false
        }
    }

    fun sendTime() {
        stompService.toHacker(sessionService.currentUserId, ReduxActions.SERVER_TIME_SYNC, ZonedDateTime.now())
    }

    fun disconnect() {
        val currentUser = sessionService.currentUser
        if (currentUser.type == UserType.USER) {

            val state = hackerStateService.retrieveForCurrentUser()

            if (state.generalActivity == HackerGeneralActivity.RUNNING) {
                notifyLeaveRun(state.userId, state.runId!!)
            }

            timedEventQueue.removeAllFor(state.userId)
            tracingPatrollerService.disconnected(state.userId)

            hackerStateService.goOffline(state)
        }
    }

    private fun notifyLeaveRun(userId: String, runId: String) {
        class HackerLeaveNotification(val userId: String)

        stompService.toRun(runId, ReduxActions.SERVER_HACKER_LEAVE_SCAN, HackerLeaveNotification(userId))
    }


}