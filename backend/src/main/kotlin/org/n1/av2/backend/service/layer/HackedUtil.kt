package org.n1.av2.backend.service.layer

import org.n1.av2.backend.model.db.run.LayerStatus
import org.n1.av2.backend.model.db.site.Node
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.service.SessionService
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.run.LayerStatusService
import org.n1.av2.backend.service.run.NodeStatusService
import org.n1.av2.backend.service.site.NodeService
import org.n1.av2.backend.util.nodeIdFromServiceId


@org.springframework.stereotype.Service
class HackedUtil(
        val layerStatusService: LayerStatusService,
        val session: SessionService,
        val nodeService: NodeService,
        val nodeStatusService: NodeStatusService,
        val stompService: StompService) {

    data class LayerHackedUpdate(val layerId: String, val nodeId: String)
    data class NodeHacked(val nodeId: String, val delay: Int)

    fun nonIceHacked(layerId: String, node: Node, runId: String) {
        val layerStatus: LayerStatus = layerStatusService.getLayerStatusOrNull(layerId, runId) ?: error("LayerStatus not found for: ${layerId}")

        saveLayerStatusHacked(layerStatus)
        val update = LayerHackedUpdate(layerStatus.layerId, node.id)
        stompService.toRun(layerStatus.runId, ReduxActions.SERVER_LAYER_HACKED, update)
    }

    fun iceHacked(puzzleId: String, delay: Int) {
        val layerStatus = layerStatusService.getForPuzzle(puzzleId)
        val nodeId = nodeIdFromServiceId(layerStatus.layerId)
        val node = nodeService.getById(nodeId)

        saveLayerStatusHacked(layerStatus)

        val update = LayerHackedUpdate(layerStatus.layerId, node.id)
        stompService.toRun(layerStatus.runId, ReduxActions.SERVER_LAYER_HACKED, update)

        val lastNonHackedIceLayerId = findLastNonHackedIceLayerId(node, layerStatus.runId)
        if (layerStatus.layerId == lastNonHackedIceLayerId) {
            nodeStatusService.createHackedStatus(node.id, layerStatus.runId)
            val nodeHackedUpdate = NodeHacked(node.id, delay)
            stompService.toRun(layerStatus.runId, ReduxActions.SERVER_NODE_HACKED, nodeHackedUpdate)
        }
    }

    private fun saveLayerStatusHacked(layerStatus: LayerStatus) {
        layerStatus.hackedBy.add(session.currentUserId)
        layerStatus.hacked = true
        layerStatusService.save(layerStatus)
    }

    private fun findLastNonHackedIceLayerId(node: Node, runId: String): String? {
        val iceLayerIds = node.layers.filter {it.type.ice }. map { it.id }
        val layerStatuses = layerStatusService.getLayerStatuses(iceLayerIds, runId)
        val hackedLayerIds = layerStatuses.filter { it.hacked } .map { it.layerId }
        val nonHackedIceLayerIds = iceLayerIds.subtract(hackedLayerIds)

        return if (nonHackedIceLayerIds.size == 1) {
            nonHackedIceLayerIds.first()
        }
        else {
            null
        }
    }


}