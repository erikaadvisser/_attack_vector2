package org.n1.av2.backend.repo

import org.n1.av2.backend.model.db.puzzle.PasswordPuzzle
import org.n1.av2.backend.model.db.puzzle.TanglePuzzle
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface TanglePuzzleRepo : PagingAndSortingRepository<TanglePuzzle, String> {
}

@Repository
interface PasswordPuzzleRepo : PagingAndSortingRepository<PasswordPuzzle, String> {
}

