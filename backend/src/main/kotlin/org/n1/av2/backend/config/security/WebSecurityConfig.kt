package org.n1.av2.backend.config.security

import org.n1.av2.backend.config.authentication.AuthenticationFilter
import org.n1.av2.backend.model.db.user.ROLE_ADMIN
import org.n1.av2.backend.model.db.user.ROLE_HACKER
import org.n1.av2.backend.model.db.user.ROLE_SITE_MANAGER
import org.n1.av2.backend.model.db.user.ROLE_USER
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


/**
 * Security config for the application
 */
@Configuration
@EnableWebSecurity
class WebSecurityConfig(val authenticationFilter: AuthenticationFilter)
    : WebSecurityConfigurerAdapter() {


    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
//                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//            .and()
            .authorizeRequests()
                .antMatchers("/", "/health",
                        "/about", "/error", "/api/login",
                        "/login", "/logoff").permitAll()
                .antMatchers("/attack_vector_websocket").hasAnyAuthority(ROLE_USER.authority)
                .antMatchers("/hacker/**").hasAnyAuthority(ROLE_HACKER.authority)
                .antMatchers("/gm/**", "/edit/**").hasAnyAuthority(ROLE_SITE_MANAGER.authority)
                .antMatchers("/api/admin/**").hasAnyAuthority(ROLE_ADMIN.authority)
            .and()
                .csrf()
                .disable()

        http.addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
    }




}