package org.n1.av2.backend.model.db

import java.time.LocalDateTime

data class ActionLog(
        val id: String,
        val userId: String,
        val timestamp: LocalDateTime,
        val path: String,
        val body: String
)