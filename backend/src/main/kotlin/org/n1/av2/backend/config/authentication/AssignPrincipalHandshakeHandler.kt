package org.n1.av2.backend.config.authentication

import org.n1.av2.backend.config.SessionType
import org.n1.av2.backend.config.determineSessionType
import org.n1.av2.backend.model.db.user.UserSession
import org.n1.av2.backend.model.iam.SessionPrincipal
import org.n1.av2.backend.service.puzzle.PuzzleSessionService
import org.n1.av2.backend.service.user.UserConnectionService
import org.springframework.http.server.ServerHttpRequest
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.server.support.DefaultHandshakeHandler
import java.security.Principal

/**
 * determineUser is called when the Websocket connection is made.
 * It adds the unique client ID to the connection in order to check that every user has only one connection.
 */
@Component
class AssignPrincipalHandshakeHandler : DefaultHandshakeHandler() {

    lateinit var userConnectionService: UserConnectionService
    lateinit var puzzleSessionService: PuzzleSessionService

    override fun determineUser(request: ServerHttpRequest, wsHandler: WebSocketHandler?,
                               attributes: Map<String, Any>?): Principal {

        val principal = SecurityContextHolder.getContext().authentication as? SessionPrincipal  ?: error("Login please");

        // The path of the page is stored in the request as the query
        val path = request.uri.query

        val sessionType = determineSessionType(path, principal)
        when (sessionType) {
            SessionType.GM_ADMIN -> {
                // always good
            }
            SessionType.HACKER -> {
                val validConnection = userConnectionService.connect(principal)
                if (!validConnection) {
                    // Throwing an error here would cause the client to receive a 500. We want to do more: convey the problem.
                    // By invalidating the pricipal, it will return a user-name that indicates the problem. And it will allow us to treat this
                    // as an invalid connection from now on.
                    principal.invalidate()
                }
            }
            SessionType.MINI_GAME -> {
                val puzzleTag = path.substringAfterLast("/")
                if (!puzzleTag.startsWith("t")) {
                    error("Unsupported puzzle tag: $puzzleTag")
                }
                val puzzleId = "tangle-${puzzleTag.substring(1,5)}"
                val userSession = UserSession(principal)
                puzzleSessionService.connect(userSession, puzzleId)
            }
            else -> {
                error("unknown session type: ${sessionType}")
            }

        }


        return principal
    }

    fun authenticateForStandalone() {
        // FIXME
    }


}