package org.n1.av2.backend.model.db.puzzle

enum class PuzzleUsageType {
    ICE, STANDALONE
}