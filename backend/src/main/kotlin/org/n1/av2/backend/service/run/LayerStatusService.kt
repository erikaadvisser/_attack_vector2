package org.n1.av2.backend.service.run

import org.n1.av2.backend.model.db.run.LayerStatus
import org.n1.av2.backend.model.db.site.Node
import org.n1.av2.backend.repo.LayerStatusRepo
import org.n1.av2.backend.util.createId
import org.springframework.stereotype.Service
import java.util.*

@Service
class LayerStatusService(
        private val layerStatusRepo: LayerStatusRepo
) {

    fun getLayerStatusOrNull(layerId: String, runId: String): LayerStatus? {
        return layerStatusRepo.findByLayerIdAndRunId(layerId, runId)
    }

    fun getOrCreate(layerId: String, runId: String, puzzleCreationFunction: () -> String): LayerStatus {
        return layerStatusRepo.findByLayerIdAndRunId(layerId, runId) ?: createLayerStatus(layerId, runId, puzzleCreationFunction)
    }

    private fun createLayerStatus(layerId: String, runId: String, puzzleCreationFunction: () -> String): LayerStatus {
        val id = createId("layerStatus-", layerStatusRepo::findById)
        val puzzleId = puzzleCreationFunction()
        val status = LayerStatus(id, layerId, runId, puzzleId,false, LinkedList())
        layerStatusRepo.save(status)
        return status
    }

    fun save(layerStatus: LayerStatus) {
        layerStatusRepo.save(layerStatus)
    }

    fun getLayerStatuses(iceLayerIds: List<String>, runId: String): List<LayerStatus> {
        return layerStatusRepo.findByRunIdAndLayerIdIn(runId, iceLayerIds)
    }

    fun getLayerStatuses(node: Node, runId: String): List<LayerStatus> {
        return getLayerStatuses(node.layers.map { it.id }, runId)
    }


    fun getForRun(runId: String): List<LayerStatus> {
        return layerStatusRepo.findByRunId(runId)
    }

    fun getForPuzzle(puzzleId: String): LayerStatus {
        return layerStatusRepo.findByPuzzleId(puzzleId) ?: error("Did not find layerStatus puzzle: ${puzzleId}")
    }
}