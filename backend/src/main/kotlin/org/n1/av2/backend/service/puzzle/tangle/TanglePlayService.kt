package org.n1.av2.backend.service.puzzle.tangle

import mu.KLogging
import org.n1.av2.backend.model.db.puzzle.PuzzleUsageType
import org.n1.av2.backend.model.db.puzzle.TanglePuzzle
import org.n1.av2.backend.model.ui.NotyMessage
import org.n1.av2.backend.model.ui.NotyType
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.repo.TanglePuzzleRepo
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.layer.HackedUtil
import org.n1.av2.backend.service.layer.ice.tangle.PADDING
import org.n1.av2.backend.service.layer.ice.tangle.X_SIZE
import org.n1.av2.backend.service.layer.ice.tangle.Y_SIZE
import org.n1.av2.backend.web.ws.puzzle.TangleController
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import kotlin.system.measureTimeMillis


data class TanglePointMoved(val puzzleId: String, val id: String,
                            val x: Int, val y: Int,
                            val status: TangleStatus)

data class TanglePuzzleSolved(val puzzleId: String)

class TangleLineSegment(val lineId: String,
                        val point1Id: String, val x1: Int, val y1: Int,
                        val point2Id: String, val x2: Int, val y2: Int)

class TangleStatus(val intersectingPointIds: Collection<String>,
                   val halfwayPointIds: Collection<String>,
                   val intersectingLineIds: Collection<String>)

@Service
class TanglePuzzlePlayService(
        val puzzleRepo: TanglePuzzleRepo,
        val hackedUtil: HackedUtil,
        val stompService: StompService
) {
    companion object : KLogging()

    private fun getPuzzle(puzzleId: String): TanglePuzzle {
        return puzzleRepo.findByIdOrNull(puzzleId) ?: error("puzzle not found: ${puzzleId}")
    }

    fun move(command: TangleController.TanglePointMoveInput) {
        val newX = keepInPlayArea(command.newX, X_SIZE)
        val newY = keepInPlayArea(command.newY, Y_SIZE)

        val puzzle = getPuzzle(command.puzzleId)

        val point = puzzle.points.first { it.id == command.pointId }
        val oldX = point.x
        val oldY = point.y
        point.x = newX
        point.y = newY

        val status = calculatePuzzleStatus(puzzle)
        val solvedAfterMove = status.intersectingLineIds.isEmpty()
        val solvedBeforeMove = puzzle.solved;

        if (solvedBeforeMove && !solvedAfterMove) {
            // Cannot unsolve a puzzle once its solved. Undo move
            stompService.toConnection(NotyMessage(NotyType.NEUTRAL, "Forbidden",
                    "Puzzle solved. Cannot make a move that un-solves the puzzle."));
            val message = TanglePointMoved(command.puzzleId, command.pointId, oldX, oldY, status)
            stompService.toPuzzle(command.puzzleId, ReduxActions.SERVER_TANGLE_POINT_MOVED, message)
            return;
        }

        puzzle.solved = solvedAfterMove
        puzzleRepo.save(puzzle)


        val message = TanglePointMoved(command.puzzleId, command.pointId, point.x, point.y, status)
        stompService.toPuzzle(command.puzzleId, ReduxActions.SERVER_TANGLE_POINT_MOVED, message)

        if (solvedAfterMove && !solvedBeforeMove) {
            val solvedMessage = TanglePuzzleSolved(puzzle.id)
            stompService.toPuzzle(command.puzzleId, ReduxActions.SERVER_TANGLE_SOLVED, solvedMessage)
            if (puzzle.type == PuzzleUsageType.ICE) {
                hackedUtil.iceHacked(puzzle.id, 70)
            }
        }
    }

    private fun keepInPlayArea(position: Int, size: Int): Int {
        if (position > size - PADDING) return size - PADDING
        if (position < PADDING) return PADDING
        return position
    }


    fun calculatePuzzleStatus(puzzle: TanglePuzzle): TangleStatus {
        val segments = toSegments(puzzle)

        var intersectingSegments: Collection<TangleLineSegment>? = null
        val millis = measureTimeMillis {
            intersectingSegments = tangleSolvedInternal(segments)
        }
        logger.debug("Measure intersections of ${puzzle.points.size} took ${millis} millis")

        return deriveTangleStatus(puzzle, intersectingSegments!!)
    }

    private fun deriveTangleStatus(puzzle: TanglePuzzle, intersectingSegments: Collection<TangleLineSegment>): TangleStatus {
        val intersectingLineIds = intersectingSegments.map { it.lineId }
        val intersectingPointIds = HashSet<String>()
        intersectingSegments.forEach {
            intersectingPointIds.add(it.point1Id)
            intersectingPointIds.add(it.point2Id)
        }

        val allLineIds = puzzle.lines.map { it.id }
        val nonIntersectingLineIds = allLineIds.subtract(intersectingLineIds)
        val nonIntersectingLines = puzzle.lines.filter { it.id in nonIntersectingLineIds }
        val pointsWithAtLeaseOneGoodLine = nonIntersectingLines.map { it.fromId }.plus( nonIntersectingLines.map { it.toId })
        val halfwayPointIds = pointsWithAtLeaseOneGoodLine.intersect(intersectingPointIds)

        intersectingPointIds.removeAll(halfwayPointIds)

        return TangleStatus(intersectingPointIds, halfwayPointIds, intersectingLineIds)
    }


    private fun toSegments(puzzle: TanglePuzzle): List<TangleLineSegment> {
        return puzzle.lines.map { line ->
            val from = puzzle.points.find { it.id == line.fromId }!!
            val to = puzzle.points.find { it.id == line.toId }!!

            TangleLineSegment(line.id, from.id, from.x, from.y, to.id, to.x, to.y)
        }
    }

    private fun tangleSolvedInternal(segments: List<TangleLineSegment>): Collection<TangleLineSegment> {
        val uncheckedSegments = segments.toMutableList()

        val intersectingSegments = HashSet<TangleLineSegment>()

        while (!uncheckedSegments.isEmpty()) {
            val segment_1 = uncheckedSegments.removeAt(0)

            uncheckedSegments.forEach { segment_2 ->
                if (!connected(segment_1, segment_2)) {
                    val intersect = segmentsIntersect(
                            segment_1.x1, segment_1.y1, segment_1.x2, segment_1.y2,
                            segment_2.x1, segment_2.y1, segment_2.x2, segment_2.y2)

                    if (intersect) {
                        intersectingSegments.add(segment_1)
                        intersectingSegments.add(segment_2)
                    }
                }
            }
        }
        return intersectingSegments
    }

    private fun connected(segment_1: TangleLineSegment, segment_2: TangleLineSegment): Boolean {
        return connected(segment_1.x1, segment_1.y1, segment_2) || connected(segment_1.x2, segment_1.y2, segment_2)
    }

    private fun connected(x: Int, y: Int, segment: TangleLineSegment): Boolean {
        return (x == segment.x1 && y == segment.y1) ||
                (x == segment.x2 && y == segment.y2)
    }

}