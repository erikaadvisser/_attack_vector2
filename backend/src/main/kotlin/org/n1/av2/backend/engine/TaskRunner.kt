package org.n1.av2.backend.engine

import mu.KLogging
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.model.ui.ValidationException
import org.n1.av2.backend.service.SessionService
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.util.FatalException
import org.n1.av2.backend.util.ServerFatal
import java.util.concurrent.LinkedBlockingQueue

class TaskRunner(val queue: LinkedBlockingQueue<Task>,
                 val stompService: StompService,
                 val sessionService: SessionService): Runnable {

    companion object: KLogging()

    var running = true

    override fun run() {
        try {
            while (running) {
                runTask()
            }
        } catch (e: InterruptedException) {
            // we were interrupted by shutdownNow(), restore interrupted status and exit
            Thread.currentThread().interrupt()
        }
    }


    private fun runTask() {
        val task = queue.take()
        try {
            sessionService.set(task.userSession)
            task.action()
        }
        catch (exception: Exception) {
            if (exception is InterruptedException) {
                throw exception
            }
            if (exception is ValidationException) {
                stompService.toConnection(exception.getNoty())
                return
            }
            if (exception is FatalException) {
                val event = ServerFatal(false, exception.message ?: "-")
                stompService.toHacker(ReduxActions.SERVER_ERROR, event)
                logger.warn("${task.userSession.name}: ${exception}")
                return
            }
            if (!sessionService.isSystemUser) {
                val event = ServerFatal(true, exception.message ?: "-")
                stompService.toHacker(ReduxActions.SERVER_ERROR, event)
                logger.info("${task.userSession.name} - task triggered exception. ", exception)
            }
            else {
                logger.info("SYSTEM - task triggered exception. ", exception)
            }
        }
        finally {
            sessionService.remove()
        }
    }

    fun terminate() {
        this.running = false
    }

}
