package org.n1.av2.backend.config

import mu.KLogging
import org.n1.av2.backend.config.authentication.AssignPrincipalHandshakeHandler
import org.n1.av2.backend.engine.SerializingExecutor
import org.n1.av2.backend.model.iam.SessionPrincipal
import org.n1.av2.backend.repo.ActionLogRepo
import org.n1.av2.backend.service.puzzle.PuzzleSessionService
import org.n1.av2.backend.service.user.UserConnectionService
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import org.springframework.messaging.simp.config.ChannelRegistration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.security.messaging.web.socket.server.CsrfTokenHandshakeInterceptor
import org.springframework.stereotype.Component
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.messaging.SessionConnectEvent
import org.springframework.web.socket.messaging.SessionDisconnectEvent
import org.springframework.web.socket.messaging.SessionSubscribeEvent
import javax.annotation.PostConstruct


@Configuration
@EnableWebSocketMessageBroker
@Component
class StompConfig(
        val assignPrincipalHandshakeHandler: AssignPrincipalHandshakeHandler,
        val actionLogRepo: ActionLogRepo) : WebSocketMessageBrokerConfigurer {

    companion object : KLogging()

    lateinit var executor: SerializingExecutor
    lateinit var userConnectionService: UserConnectionService

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry
                .addEndpoint("/attack_vector_websocket")
                .setAllowedOrigins("*")
                .setHandshakeHandler(assignPrincipalHandshakeHandler)
                .addInterceptors(CsrfTokenHandshakeInterceptor())
    }

    override fun configureMessageBroker(registry: MessageBrokerRegistry) {
        registry.setApplicationDestinationPrefixes("/av")
        registry.enableSimpleBroker("/topic", "/reply", "/error")
        registry.setUserDestinationPrefix("/user")
    }

    override fun configureClientInboundChannel(registration: ChannelRegistration?) {
        registration!!.interceptors(MessageIn(actionLogRepo))
    }

    // Have the StompService log this, as we get better info there: user-ids and run-ids instead of stomp channel ids.
//    override fun configureClientOutboundChannel(registration: ChannelRegistration) {
//        registration.interceptors(MessageOut())
//    }


    @EventListener
    fun handleConnectEvent(event: SessionConnectEvent) {
//        val principal = event.user!! as UserPrincipal

        // fixme
//        val validConnection = stompConnectionEventService.connect(principal)
//        if (!validConnection) {
//            principal.invalidate()
//        }
        logger.debug { "in > connect ${event.user!!.name}" }
    }

    @EventListener
    fun handleDisconnectEvent(event: SessionDisconnectEvent) {
        val userPrincipal = event.user!! as SessionPrincipal
        if (!userPrincipal.invalidated) {
            executor.run(userPrincipal) {
                userConnectionService.disconnect()
            }
            logger.debug { "<out disconnect ${userPrincipal.name}" }
        }

    }

    @EventListener
    fun handleSubscribeEvent(event: SessionSubscribeEvent) {
        logger.debug { "in > subscribe ${event.user!!.name} ${event.message.headers["nativeHeaders"]}" }
        val principal = event.user!! as SessionPrincipal
        executor.run(principal) {
            userConnectionService.sendTime()
        }
    }
}

@Configuration
class ConfigureStompServiceAndHackerActivityService(
        val executor: SerializingExecutor,
        val userConnectionService: UserConnectionService,
        val assignPrincipalHandshakeHandler: AssignPrincipalHandshakeHandler,
        val puzzleSessionService: PuzzleSessionService,
        val stompConfig: StompConfig) {

    @PostConstruct
    fun postConstruct() {
        stompConfig.executor = executor
        stompConfig.userConnectionService = userConnectionService
        assignPrincipalHandshakeHandler.userConnectionService = userConnectionService
        assignPrincipalHandshakeHandler.puzzleSessionService = puzzleSessionService
    }

}
