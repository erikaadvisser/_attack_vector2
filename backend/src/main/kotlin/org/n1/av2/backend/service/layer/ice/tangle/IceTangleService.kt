package org.n1.av2.backend.service.layer.ice.tangle

import mu.KLogging
import org.n1.av2.backend.model.db.layer.IceTangleLayer
import org.n1.av2.backend.model.db.puzzle.PuzzleUsageType
import org.n1.av2.backend.model.db.puzzle.TanglePuzzle
import org.n1.av2.backend.model.db.site.Node
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.puzzle.TanglePuzzleService
import org.n1.av2.backend.service.puzzle.tangle.TanglePuzzlePlayService
import org.n1.av2.backend.service.puzzle.tangle.TangleStatus
import org.n1.av2.backend.service.run.LayerStatusService
import org.springframework.stereotype.Service

const val X_SIZE = 1200
const val Y_SIZE = 680
const val PADDING = 10



data class TangleIceStart(
        val puzzle: TanglePuzzle,
        val status: TangleStatus)


@Service
class IceTangleService(
        private val tanglePuzzleService: TanglePuzzleService,
        private val tanglePuzzlePlayService: TanglePuzzlePlayService,
        private val layerStatusService: LayerStatusService,
        private val stompService: StompService) {


    companion object : KLogging()

    fun hack(node: Node, layer: IceTangleLayer, runId: String) {
        val puzzle = getOrCreatePuzzle(node, layer, runId)

        val status = tanglePuzzlePlayService.calculatePuzzleStatus(puzzle)
        val message = TangleIceStart(puzzle, status)

        stompService.toHacker(ReduxActions.SERVER_START_HACKING_ICE_TANGLE, message)
    }

    private fun getOrCreatePuzzle(node: Node, layer: IceTangleLayer, runId: String): TanglePuzzle {
        val layerStatus = layerStatusService.getOrCreate(layer.id, runId) {
            val description = "${runId} ${layer.id} "
            val puzzle = tanglePuzzleService.create(layer.strength, PuzzleUsageType.ICE, "system", description)
            puzzle.id
        }
        return tanglePuzzleService.getPuzzle(layerStatus.puzzleId!!)
    }
}