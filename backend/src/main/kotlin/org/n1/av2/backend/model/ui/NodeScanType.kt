package org.n1.av2.backend.model.ui

enum class NodeScanType {
    SCAN_NODE_INITIAL,
    SCAN_CONNECTIONS,
    SCAN_NODE_DEEP
}