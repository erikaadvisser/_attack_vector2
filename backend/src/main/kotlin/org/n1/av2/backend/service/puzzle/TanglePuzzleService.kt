package org.n1.av2.backend.service.puzzle

import org.n1.av2.backend.model.db.puzzle.PuzzleStrength
import org.n1.av2.backend.model.db.puzzle.PuzzleUsageType
import org.n1.av2.backend.model.db.puzzle.TanglePuzzle
import org.n1.av2.backend.model.db.run.PlayerPresenceState
import org.n1.av2.backend.model.db.user.HackerIcon
import org.n1.av2.backend.model.db.user.User
import org.n1.av2.backend.model.ui.NotyMessage
import org.n1.av2.backend.model.ui.NotyType
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.repo.TanglePuzzleRepo
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.puzzle.tangle.TangleCreator
import org.n1.av2.backend.service.puzzle.tangle.TanglePuzzlePlayService
import org.n1.av2.backend.service.puzzle.tangle.TangleStatus
import org.n1.av2.backend.util.createId
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class TanglePuzzleService(
        val puzzleRepo: TanglePuzzleRepo,
        val tanglePuzzlePlayService: TanglePuzzlePlayService,
        val puzzleSessionService: PuzzleSessionService,
        val stompService: StompService) {


    fun getPuzzle(id: String): TanglePuzzle {
        return puzzleRepo.findByIdOrNull(id) ?: error("Tangle puzzle not found for id: ${id}")
    }

    fun create(strength: PuzzleStrength, type: PuzzleUsageType, owner: String, description: String, idInput: String? = null): TanglePuzzle {
        val creation = TangleCreator().create(strength)
        val puzzle = TanglePuzzle(
                id = idInput ?: createId("tangle", puzzleRepo::findById),
                type = type,
                strength = strength,
                description = description,
                owner = owner,
                lines = creation.lines,
                points = creation.points,
                solved = false
                )
        puzzleRepo.save(puzzle)
        return puzzle
    }



    data class TangleInfo(val id: String, val strength: PuzzleStrength, val description: String, val activePlayers: Int, val type: PuzzleUsageType )

    fun findAll() {
        val infos = puzzleRepo.findAll().map {
            puzzle -> TangleInfo(puzzle.id, puzzle.strength, puzzle.description, 0, puzzle.type)
        }
        stompService.toHacker(ReduxActions.SERVER_LIST_TANGLES, infos)
    }


    fun deleteAll() {
        puzzleRepo.deleteAll()
    }

    class PlayerPresence(val userId: String,
                         val name: String,
                         val icon: HackerIcon,
                         val isYou: Boolean)

    class TanglePuzzleStart(
            val puzzle: TanglePuzzle,
            val status: TangleStatus,
            val players: List<PlayerPresence>)


    fun demo(puzzleId: String) {
        val puzzle = puzzleRepo.findByIdOrNull(puzzleId) ?: createDemoPuzzle(puzzleId)
        val status = tanglePuzzlePlayService.calculatePuzzleStatus(puzzle)

        val presence = puzzleSessionService.forPuzzle(puzzleId)

        val players = presence.players.map { entry: Map.Entry<User, PlayerPresenceState> ->
            PlayerPresence(entry.key.id, entry.key.name, entry.key.icon, false)
        }

        val message = TanglePuzzleStart(puzzle, status, players)

        stompService.toConnection(ReduxActions.SERVER_START_TANGLE_STANDALONE, message)

    }

    fun createDemoPuzzle(puzzleId: String): TanglePuzzle {
        val strength = when (puzzleId.substring(7, 8)) {
            "1" -> PuzzleStrength.VERY_WEAK
            "2" -> PuzzleStrength.WEAK
            "3" -> PuzzleStrength.AVERAGE
            "4" -> PuzzleStrength.STRONG
            "5" -> PuzzleStrength.VERY_STRONG
            "6" -> PuzzleStrength.IMPENETRABLE
            else ->  PuzzleStrength.WEAK
        }

        val puzzle = create(strength, PuzzleUsageType.STANDALONE, "demo", "demo", puzzleId)
        return puzzle
    }

    fun demoRestart(puzzleId: String) {
        puzzleRepo.deleteById(puzzleId)
        val puzzle = createDemoPuzzle(puzzleId)
        puzzleRepo.save(puzzle)
        val status = tanglePuzzlePlayService.calculatePuzzleStatus(puzzle)
        val message = TanglePuzzleStart(puzzle, status, emptyList())

        stompService.toPuzzle(puzzleId, ReduxActions.SERVER_START_TANGLE_STANDALONE, message)
        stompService.toPuzzle(puzzleId, ReduxActions.SERVER_NOTIFICATION, NotyMessage(NotyType.OK, "!", "New puzzle started"))
    }


}
