package org.n1.av2.backend.web.ws.puzzle

import org.n1.av2.backend.engine.SerializingExecutor
import org.n1.av2.backend.service.puzzle.PasswordPuzzleService
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import java.security.Principal

@Controller
class IcePasswordController(
        val passwordPuzzleService: PasswordPuzzleService,
        val executor: SerializingExecutor ) {


    class SubmitPassword(val puzzleId: String, val password: String)
    @MessageMapping("/ice/password/submit")
    fun scansOfPlayer(command: SubmitPassword, principal: Principal) {
        executor.run(principal) { passwordPuzzleService.submitAttempt(command) }
    }


}