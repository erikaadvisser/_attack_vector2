package org.n1.av2.backend.service

import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets
import kotlin.random.Random

/** This service generates random hacker names */
@Service
class HackerNameService {

    val names: List<String> = HackerNameService::class.java.getResource("/services/hacker-names.txt")
            .readText(StandardCharsets.UTF_8)
            .lines()

    val random = Random

    fun randomName(): String {
        val i = random.nextInt(names.size)
        return names[i]
    }

}