package org.n1.av2.backend.web.rest

import org.n1.av2.backend.config.authentication.JwtTokenProvider
import org.n1.av2.backend.model.db.user.User
import org.n1.av2.backend.service.CookiesService
import org.n1.av2.backend.service.user.UserService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

val LOGIN_EXPIRY_S: Int = 60 * 60 * 48 // 2 days


@RestController
@RequestMapping("/api")
class LoginController(
                      private val userService: UserService,
                      private val jwtTokenProvider: JwtTokenProvider,
                      private val cookiesService: CookiesService) {

    data class LoginInput(val name: String = "", val password: String = "")
    data class LoginResponse(val success:Boolean, val message: String? = null)

    @PostMapping("/login")
    fun login(@RequestBody input: LoginInput, response: HttpServletResponse): LoginResponse {
        try {
            val user = userService.login(input.name, input.password)
            val jwt = jwtTokenProvider.generateJwt(user, LOGIN_EXPIRY_S)

            cookiesService.addCookies(user, jwt, response, LOGIN_EXPIRY_S)

            return LoginResponse(true )
        }
        catch (exception: UsernameNotFoundException) {
            return LoginResponse(false, "UseName or password invalid")
        }
    }

    data class AnonymousSignUpInput(val name: String = "", val icon: String = "")

    @PostMapping("/signUpAnonymous")
    fun signUpAnonymous(@RequestBody input: AnonymousSignUpInput, response: HttpServletResponse): LoginResponse {
        try {
            val user = userService.signUpAnonymous(input.name, input.icon)
            val jwt = jwtTokenProvider.generateJwt(user, LOGIN_EXPIRY_S)

            cookiesService.addCookies(user, jwt, response, LOGIN_EXPIRY_S)

            return LoginResponse(true )
        }
        catch (exception: UsernameNotFoundException) {
            return LoginResponse(false, "UseName or password invalid")
        }
    }
}