package org.n1.av2.backend.config.authentication

import io.jsonwebtoken.*
import mu.KLogging
import org.n1.av2.backend.model.db.user.User
import org.springframework.stereotype.Component
import java.nio.charset.StandardCharsets
import java.security.SignatureException
import java.util.*
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec


@Component
class JwtTokenProvider {

    private val jwtSecret = "SuperSecretKeyForHS512NeedsToBeLongEnoughToContainAtLeast512BitsOfEntropySoThisShouldDoIt".toByteArray(StandardCharsets.UTF_8)
    private val key: SecretKey = SecretKeySpec(jwtSecret, SignatureAlgorithm.HS512.jcaName)
    private val parser = Jwts.parser().setSigningKey(jwtSecret)


    companion object : KLogging()

    fun generateJwt(user: User, cookieExpirationInS: Int): String {


        val now = Date()
        val expiryDate = Date(now.time + cookieExpirationInS * 1000L)

        return Jwts.builder()
                .setSubject(user.id)
                .setIssuedAt(Date())
                .setExpiration(expiryDate)
                .signWith(key)
                .compact()
    }

    fun validateToken(authToken: String): Claims? {
        try {
            return parser.parseClaimsJws(authToken).body
        } catch (ex: SignatureException) {
            logger.error("Invalid JWT signature for token: ${authToken}")
        } catch (ex: MalformedJwtException) {
            logger.error("Invalid JWT token for: ${authToken}")
        } catch (ex: ExpiredJwtException) {
            logger.error("Expired JWT token for: ${authToken}")
        } catch (ex: UnsupportedJwtException) {
            logger.error("Unsupported JWT token for: ${authToken}")
        } catch (ex: IllegalArgumentException) {
            logger.error("JWT claims string is empty for token: ${authToken}")
        }

        return null
    }

}

val Claims.userId: String
   get() = this.subject


