package org.n1.av2.backend.config.authentication

import mu.KLogging
import org.n1.av2.backend.model.iam.SessionPrincipal
import org.n1.av2.backend.service.CookiesService
import org.n1.av2.backend.service.HackerNameService
import org.n1.av2.backend.service.SessionService
import org.n1.av2.backend.service.user.UserService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

const val EXPIRY_ANONYMOUS_LOGIN_S = 365 * 24 * 60 * 60

@Component
class AuthenticationFilter(
        private val jwtTokenProvider: JwtTokenProvider,
        private val userService: UserService,
        private val hackerNameService: HackerNameService,
        private val sessionService: SessionService,
        private val cookiesService: CookiesService
) : OncePerRequestFilter() {

    companion object : KLogging()

    private val resourceExtensions = setOf("js", "html", "css", "png", "jpg", "ico", "woff",
            "eot", "svg", "ttf", "woff2", "json", "map")

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        if (needAuthentication(request)) {
            tryAuthenticateViaJwt(request)
        }

        filterChain.doFilter(request, response)
    }

    private fun needAuthentication(request: HttpServletRequest): Boolean {
        val extension = request.requestURL.toString().substringAfterLast(".")
        if (resourceExtensions.contains(extension.toLowerCase())) {
            // resources don't need authentication
            return false
        }

        if (request.requestURL.toString().endsWith("/api/login")) {
            // Don't interfere with login process
          return false
        }

        return true
    }

    private fun tryAuthenticateViaJwt(request: HttpServletRequest) {
        try {
            val jwt = getJwtFromRequest(request)

            if (jwt != null) {
                val claims = jwtTokenProvider.validateToken(jwt)
                if (claims != null) {
                    val user = userService.getById(claims.userId)
                    val authentication = SessionPrincipal(user)
                    SecurityContextHolder.getContext().authentication = authentication
                }
            }
        } catch (exception: Exception) {
            logger.warn("Failed to parse JWT", exception)
        }
    }

    private fun getJwtFromRequest(request: HttpServletRequest): String? {
        if (request.requestURI == "/api/health/") {
            return null
        }
        val cookie = request.cookies?.find { it.name == "jwt" } ?: return null
        return cookie.value
    }

}