package org.n1.av2.backend.service.layer.ice.password

import org.n1.av2.backend.model.db.layer.IcePasswordLayer
import org.n1.av2.backend.model.db.puzzle.PasswordPuzzle
import org.n1.av2.backend.model.db.puzzle.PuzzleUsageType
import org.n1.av2.backend.model.db.site.Node
import org.n1.av2.backend.model.ui.ReduxActions
import org.n1.av2.backend.service.StompService
import org.n1.av2.backend.service.TimeService
import org.n1.av2.backend.service.puzzle.PasswordPuzzleService
import org.n1.av2.backend.service.run.LayerStatusService
import org.n1.av2.backend.service.site.NodeService
import org.springframework.stereotype.Service

@Service
class IcePasswordService(
        val passwordPuzzleService: PasswordPuzzleService,
        val nodeService: NodeService,
        val time: TimeService,
        val layerStatusService: LayerStatusService,
        val stompService: StompService) {


    data class UiState(val puzzle: PasswordPuzzle, val message: String?)
    fun hack(node: Node, layer: IcePasswordLayer, runId: String) {
        val puzzle = getOrCreatePuzzle(node, layer, runId)
        val uiState = UiState( puzzle, null)
        stompService.toHacker(ReduxActions.SERVER_START_HACKING_ICE_PASSWORD, uiState)
    }

    private fun getOrCreatePuzzle(node: Node, layer: IcePasswordLayer, runId: String): PasswordPuzzle {
        val layerStatus = layerStatusService.getOrCreate(layer.id, runId) {
            val description = "Ice for run: ${runId}, node: ${node.id}|${node.networkId}, layer: ${layer.id}|${layer.level}"
            val puzzle = passwordPuzzleService.createPasswordPuzzle(layer, PuzzleUsageType.ICE, "system", description)
            puzzle.id
        }

        return passwordPuzzleService.getPuzzle(layerStatus.puzzleId!!)
    }




}