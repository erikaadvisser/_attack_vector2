import java.time.Duration

fun main(args: Array<String>) {

    val duration = Duration.ofHours(2).plusMinutes(4).plusSeconds(6)
    val hours = duration.toHours();
    val minutes = duration.toMinutes() - 60 * hours;
    val seconds = (duration.toMillis() / 1000) - duration.toMinutes() * 60;

    println( "${hours}:${minutes}:${seconds}")

}

