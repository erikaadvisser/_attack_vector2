# Security setup

Note that the goal of security is to facilitate players, and provide enough security to be able to run 
this application on a server connected directly to the internet. It is not intended to be foolproof, as
the assumption is that players will not hack it (that is cheating) and there is not a lot to gain for 
attackers from outside.

## Authentication
The primary way for a user to access the application is through websockets. 
 Unfortunately the websocket implementation of both browsers and the STOMP reference implementation
 do not provide a lot of options to authenticate outside of the websocket connection. For instance, you
 cannot add requests parameters from the browser.
 
 Authenticating over the websocket connection (after it is established) is just a bad idea from a security
 perspective, as well as leading to lots of checks and state management when parsing websocket messages.
 Things are better if they are simpler, and simpler means that once the websocket connection is established,
 we know who we are talking to. The STOMP framework agrees, you can only set the Principal as part of the
 handshake (i.e. when setting up the connection). 
 
The solution is to rely on the cookies that are provided as part of the https websocket (upgrade) request.
 This request should contain a cookie with an authentication token. If not, the user is redirected to
 the login page.
 
The login page sends username + password to the LoginController. That class checks those values  against
 the hashed values stored in the database.
 If correct, the LoginService adds several cookies to the response, all valid for 48 hours:
 - 'jwt': authentication token
 - 'userName': The user name (for display purposes)
 - 'type' UserType (Hacker, GM, ...)
 - 'roles': The roles associated with the type

The login page then redirects the browser to the original requested page, or the / page. From there
 the frontend will set up the websocket connection. Part of this connection will be the cookies just set.
 
The AuthenticationFilter will intercept the request, and check the jwt cookie. It that is present, 
 then has the JwtTokenProvider check it. If it is (still) valid, then the user is looked up from the
 database and set as the authentication. 
 (This is where spring security is fiddly with Authentication and Principal. In the code we care about the
 User object, so we wrap that in the Authentication / Principal).

Almost done... the websocket request now goes through to its actual websocket handling part. This is
STOMP code, so we have less control over it. The AssignPrincipalHandshakeHandler is used to set the
Principal for this websocket connection.

From here on, all @MessageMapping methods in the web socket controllers will pass along the Principal as part of the
request.

An advantage of this solution is that the authentication will last as long as we want it to, we can manage this
with the expiry of the cookies (and additionally in the expiry in the jwt).

## Authorization
Since the principal is set, we use Spring Security for managing what roles are required for which pages in
via the WebSecurityConfig class. Unfortunately, during development, the back end is not serving the HTML,
but that is the react dev environment itself. So these checks are not met during regular development.

Also: the frontend is one big rich client, with all functionality for all roles included. This is where the
'roles' cookie comes in. The frontend uses it to determine what pages should be seen by which users. See
the RequiresRole file in the frontend. 

## Lack of authorization
The current state of the implementation does not take roles into account when processing web socket requests.
This means that anyone who can establish a connection, can send all requests. 

We could/should fix this with @RolesAllowed annotations, but this is non-trivial. We'd need to get this
to work with STOMP / and / or our internal scheduler. We don't have a thread-per-request model here.

## Anonymous logins
For standalone minigames it is important that players can just play, and don't need to sign up first / log in every game.

Therefore, players can be granted an anonymous login when they access the standalone version of a minigame. 
The AuthenticationFilter creates an anonymous User with a user id that contains a full UUID. This authentication is 
valid for 10 years because there is no reason for it to expire.
