# Session management
In a multiplayer environment, the game engine needs to keep track of who is playing the game
and what they are doing. Unfortunately this involves manual tracking the state of each player.

## Use cases
A player can only participate as one person in each minigame. Multi player challenges should not be
 overcome by pretending to be multiple players.
 
A hacker can only have one site connection to a single site. Again to enforce multiplayer challenges. 

During a site-hack or when playing a minigame, players should see each other to support cooperation.

## Business logic for minigames
When someone connects to a minigame, they create a presence in that minigame and they are labelled as active. 

are 'at' that minigame and active. Other players can 
see that you are there and active.

### On hold & resume 
When you switch tabs (minimize browser / switch to another app on you phone) then your presence is on
 hold. Other players can see this. You can no longer participate in the puzzle even if you switch 
 back to this tab / open the app your phone. You need to 'Resume' playing by pushing the big resume
 button displayed on the screen. Also: while on hold, you dont receive updates on what is going on
 in the minigame. You see an overlay that blurs the minigame.
  
As long as you have a websocket connection, you are not disconnected. If applicable: you are still
 'at' this minigame. Your in-game state still exists, and the game decides what it means that you are 
 on hold. This also means it is faster to re-join this minigame.
 
### Disconnecting
When your websocket connection is lost, you are no longer 'at' the minigame. Your fellow players will see
this. Your in-game state is lost.

You can re-connect to the minigame and normally it treats you as if you connected for the first time 
(TDB maybe some minigames will make exceptions and store 'offline' state for a player.) 

### Restrictions
You can have only connection to a specific mini game once. If you start a second connection, then
the first one will be disconnected. If you later switch back to the tab/window of the first game, then
it will show a screen as if you were on hold. You press the 'Resume' button, to reload the page
and make this the active connection. 

 
## Business logic for site-hacking
When you start a run you have presence in the site. Other players can see you. 

### On hold, resume and disconnecting
This is similar to playing minigames.

If you switch tabs (for instance to play a minigame)
 then you are on hold in the site. This does mean very much to the site, you can still be affected by
 whatever is in the site. But other players can see that you are on hold.
 
When you switch back to the site's browser tab, you automatically resume play (but you might see
 a blurred overlay while the engine updates the site state).
 
When your websocket connection is lost/closes, you automatically disconnect from the run and your 
presence is gone. You can no longer be affected directly by the site (but maybe still indirectly by
traces started while you where there).

### Restrictions
You cannot establish a connection to the same site twice. The second connection will be refused.

For now: you cannot establish a connection to a different site while running a first site. (This may
change in the future.)
 
## Implementation
When someone establishes a websocket connection, the HackerStateService keeps track of their activities.

The front end is responsible for letting the server know if the player is on hold by sending events. 
(The browser uses the document.onvisibilitychange events. This allows a player to have a browser open and
work on another window while still being connected and active. 